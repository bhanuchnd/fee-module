export const navigation = [
  {
    id: 1,
    name: 'Dashboard',
    url: '/home/dashboard',
    icon: 'icon-home'
  },
  {
    id: 2,
    name: 'Transaction',
    url: '/home/transaction',
    icon: 'icon-wallet'
  },
  {
    id: 3,
    name: 'Discount',
    url: '/home/discount',
    icon: 'icon-speedometer'
    /* badge: {
       variant: 'info',
       text: 'NEW'
     }*/
  },
  {
    id: 4,
    name: 'Reports',
    url: '/home/reports',
    icon: 'icon-docs'
  },
  {
    id: 5,
    name: 'Communication',
    url: '/home/communication',
    icon: 'icon-envelope',
  },
  {
    title: true,
    name: 'Settings'
  },
  {
    id: 6,
    name: 'Templates',
    url: '/home/templates',
    icon: 'icon-doc',
    children: [
      {
        name: 'Classification',
        url: '/home/templates/template-classify',
        icon: 'icon-settings'
      },
      {
        name: 'Name',
        url: '/home/templates/template-name',
        icon: 'icon-puzzle'
      },
      {
        name: 'Headers',
        url: '/home/templates/template-headers',
        icon: 'icon-note'
      }
    ]
  },
  {
    id: 7,
    name: 'Students',
    url: '/home/students',
    icon: 'icon-user',
    children: [{
     name: 'New Admission',
      url: '/home/students/newadmission',
      icon: 'icon-user-follow'
    },
      {
        name: 'Map Section',
        url: '/home/students/map-section',
        icon: 'icon-user-following'
      },
      {
        name: 'View Students',
        url: '/home/students/view-students',
        icon: 'icon-people'
      }
    ]
  },
  {
    id: 8,
    name: 'Fee Structures',
    url: '/home/mapping',
    icon: 'icon-organization',
    children: [
      {
        name: 'Class-Wise',
        url: '/home/mapping/fee-structure',
        icon: 'icon-layers'
      },
      {
        name: 'Student-Wise',
        url: '/home/mapping/student-fee-structure',
        icon: 'icon-layers'
      }
    ]
  },
  {
    id:9,
    name: 'Settings',
    url: '/home/settings',
    icon: 'icon-settings'
  }


];
