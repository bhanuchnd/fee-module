import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const classifications = [
      {id: 11, name: '1 st term', schoolId: 90},
      {id: 12, name: '2 nd term', schoolId: 90},
      {id: 13, name: '3 rd term', schoolId: 90}
    ];

    const templates = [
      {id: 1, name: '1 st term 2nd class fees', schoolId: 90, tempClassRefId: 11, dueDate: new Date()},
      {id: 2, name: '2 nd term 3rd class fees', schoolId: 90, tempClassRefId: 11, dueDate: new Date()},
      {id: 3, name: '3 rd term 12th class fees', schoolId: 90, tempClassRefId: 13, dueDate: new Date()}
    ];

    const headers = [
      {id: 1, name: 'Admission Fee', schoolId: 90},
      {id: 2, name: 'Sports Fee', schoolId: 90},
      {id: 3, name: 'Tutition Fee', schoolId: 90}
    ];

    const schools = [
      {id: 1, name: 'Aychuta school'},
      {id: 2, name: 'Green Park school'},
    ]
    const classes = [
      {id: 1, name: '1st Class'},
      {id: 2, name: '2nd Class'},
      {id: 3, name: '3rd Class'},
      {id: 4, name: '4th Class'},
      {id: 5, name: '5th Class'},
      {id: 6, name: '6th Class'},
      {id: 7, name: '7th Class'}

    ]
    const students = [
      {id: 1, name: 'Yuvaraj', schoolId: 1, classId: 1, sectionId: 1},
      {id: 2, name: 'Saurabh', schoolId: 2, classId: 1, sectionId: 1},
      {id: 3, name: 'Vinoth', schoolId: 2, classId: 1, sectionId: 1},
      {id: 4, name: 'Yudister', schoolId: 1, classId: 1, sectionId: 1},
      {id: 5, name: 'Sajal', schoolId: 2, classId: 1, sectionId: 1},
      {id: 6, name: 'Varun', schoolId: 2, classId: 1, sectionId: 1},

    ];

    const discounts = [
      {id: 1, student: {id: 1, name: 'Yuvaraj', schoolId: 1, classId: 1, sectionId: 1}, templateId: 1, amount: 5000},
      {id: 2, student: {id: 2, name: 'Saurabh', schoolId: 2, classId: 1, sectionId: 1}, templateId: 1, amount: 4000},
      {id: 3, student: {id: 2, name: 'Saurabh', schoolId: 2, classId: 1, sectionId: 1}, templateId: 2, amount: 3000},
    ]

    const studentTemplates = [
      {
        studentId: 1,
        templates: [{id: 1, name: '1 st term 2nd class fees', schoolId: 90},
        {id: 2, name: '2 nd term 2nd class fees', schoolId: 90},
        {id: 3, name: '3 rd term 2nd class fees', schoolId: 90}
        ]
      }
    ]
    const studentTransactions = [
      {
        studentId: 1, studentName: 'Yuvaraj',
        totalPaidAmount: 30000,
        overDueAmount: 15000,
        transactions: {
          paidList: [
            {transId: 1, templateId: 1, amount: 3000, mode: 'cash'},
            {
              transId: 3, templateId: 1, amount: 3000, mode: 'cash'
            }
          ], overdueList: [
            {templateId: 3, overDueAmount: 3000},
            {
              templateId: 4, overDueAmount: 2000
            }
          ]
        }
      }
    ]
    const report = {
      'status': '200',
      'report': {
          'result_report': [
            {
              'studentid': '12790',
              'name': 'AAKAASH.AA',
              'class': 'I',
              'section': 'A',
              'transactions': [
                {
                'template': '2nd-Term 1st class',
                'amount': '3000',
                'amount_due': '3000',
                'date_time': '2018-04-27 17:34:41'
              },
              {
                'template': 'Class-1(picnic) 1st-term',
                'amount': '6000',
                'amount_due': '5000',
                'date_time': '2018-04-27 17:34:56'
              },
              {
                'template': '1st-Term Ist-Class',
                'amount': '3000',
                'amount_due': '3000',
                'date_time': '2018-04-28 17:40:38'
              }
            ]
            },
            {
                  'studentid': '12792',
                  'name': 'DHIVAGAR.S',
                  'class': 'I',
                  'section': 'A',
                  transactions: [
                    {
                      'template': '1st-Term Ist-Class',
                      'amount': '11200',
                      'amount_due': '0',
                      'date_time': '2018-05-02 11:15:10'
                    },
                    {
                      'template': 'School-Picnic',
                      'amount': '3000',
                      'amount_due': '7000',
                      'date_time': '2018-05-04 13:24:12'
                    }
                  ]
              },
          ],
          'total_amount': '259600.90',
          'report_name': 'FeeCollection',
          'report_type': '1',
          'header_name': 'Amount Collected'
      }
  }
  const reports = {
    'status': '200',
    'report': {
        'result_report': {
            '12790': [
                {
                    'id': '21',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '5200',
                    'amount_due': '6000',
                    'date_time': '2018-04-27 17:34:28'
                },
                {
                    'id': '22',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '3000',
                    'amount_due': '3000',
                    'date_time': '2018-04-27 17:34:41'
                },
                {
                    'id': '23',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': 'Class-1(picnic) 1st-term',
                    'amount': '6000',
                    'amount_due': '5000',
                    'date_time': '2018-04-27 17:34:56'
                },
                {
                    'id': '24',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '3000',
                    'amount_due': '3000',
                    'date_time': '2018-04-28 17:40:38'
                },
                {
                    'id': '25',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '2000',
                    'amount_due': '1000',
                    'date_time': '2018-04-28 17:40:49'
                },
                {
                    'id': '26',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': 'Class-1(picnic) 1st-term',
                    'amount': '3000',
                    'amount_due': '2000',
                    'date_time': '2018-04-28 17:41:00'
                },
                {
                    'id': '61',
                    'studentid': '12790',
                    'name': 'AAKAASH.AA',
                    'class': 'I',
                    'section': 'A',
                    'template': 'Testtt',
                    'amount': '3000',
                    'amount_due': '5000',
                    'date_time': '2018-06-21 11:36:00'
                }
            ],
            '12792': [
                {
                    'id': '43',
                    'studentid': '12792',
                    'name': 'DHIVAGAR.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '11200',
                    'amount_due': '0',
                    'date_time': '2018-05-02 11:15:10'
                },
                {
                    'id': '48',
                    'studentid': '12792',
                    'name': 'DHIVAGAR.S',
                    'class': 'I',
                    'section': 'A',
                    'template': 'School-Picnic',
                    'amount': '3000',
                    'amount_due': '7000',
                    'date_time': '2018-05-04 13:24:12'
                },
                {
                    'id': '49',
                    'studentid': '12792',
                    'name': 'DHIVAGAR.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '6000',
                    'amount_due': '0',
                    'date_time': '2018-05-07 11:41:01'
                },
                {
                    'id': '50',
                    'studentid': '12792',
                    'name': 'DHIVAGAR.S',
                    'class': 'I',
                    'section': 'A',
                    'template': 'School-Picnic',
                    'amount': '2000',
                    'amount_due': '5000',
                    'date_time': '2018-05-07 11:41:47'
                },
                {
                    'id': '58',
                    'studentid': '12792',
                    'name': 'DHIVAGAR.S',
                    'class': 'I',
                    'section': 'A',
                    'template': 'Class-1(picnic) 1st-term',
                    'amount': '1000',
                    'amount_due': '8000',
                    'date_time': '2018-05-31 15:44:39'
                }
            ],
            '12795': [
                {
                    'id': '56',
                    'studentid': '12795',
                    'name': 'UDITH NARAYANAN.T.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '11200',
                    'amount_due': '0',
                    'date_time': '2018-05-16 12:18:26'
                },
                {
                    'id': '57',
                    'studentid': '12795',
                    'name': 'UDITH NARAYANAN.T.S',
                    'class': 'I',
                    'section': 'A',
                    'template': 'School-Picnic',
                    'amount': '10000',
                    'amount_due': '0',
                    'date_time': '2018-05-16 12:19:29'
                }
            ],
            '12800': [
                {
                    'id': '2',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '200',
                    'amount_due': '11000',
                    'date_time': '2018-04-15 17:59:06'
                },
                {
                    'id': '3',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '3000',
                    'amount_due': '8000',
                    'date_time': '2018-04-16 18:47:14'
                },
                {
                    'id': '27',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '1st-Term Ist-Class',
                    'amount': '8000',
                    'amount_due': '0',
                    'date_time': '2018-05-01 12:03:42'
                },
                {
                    'id': '34',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '2000',
                    'amount_due': '4000',
                    'date_time': '2018-05-01 17:50:07'
                },
                {
                    'id': '35',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '2000',
                    'amount_due': '2000',
                    'date_time': '2018-05-01 17:50:32'
                },
                {
                    'id': '36',
                    'studentid': '12800',
                    'name': 'HARI PRAKASH.S',
                    'class': 'I',
                    'section': 'A',
                    'template': '2nd-Term 1st class',
                    'amount': '2000',
                    'amount_due': '0',
                    'date_time': '2018-05-01 17:51:00'
                }
            ]
        },
        'total_amount': '259600.90',
        'report_name': 'FeeCollection',
        'report_type': '1',
        'header_name': 'Amount Collected'
    }
}
    return {classifications, templates, headers, classes, studentTemplates,
      studentTransactions, schools, students, discounts, report, reports};
  }
}
