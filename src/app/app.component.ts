import { Component } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

 }
