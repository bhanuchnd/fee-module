/* thought of creating my own interceptor but used @auth0/anugalr-jwt interceptor.
 * 
 *  https://angular.io/guide/http#intercepting-requests-and-responses
 *  
 *  */ 

//import {Observable} from 'rxjs/Observable';
//import 'rxjs/add/operator/do';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/observable/throw';
//import 'rxjs/add/observable/empty';
//import {Injectable} from '@angular/core';
//import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,HttpResponse} from '@angular/common/http';
//import {AuthService} from '../service/auth.service';
//@Injectable()
//export class AuthenticationInterceptor implements HttpInterceptor {
//  constructor(private authService: AuthService) {
//  }
//  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//    // Set request header and middle ware stuff and then pass to httpclientmodule. 
//    return next.handle(this.setAuthorizationHeader(req))
//      .catch((event) => {
//        console.log('event', event);
//          if (event instanceof HttpResponse) {
//              console.log('response event', event);
//          } else if (event instanceof HttpErrorResponse) {
//              return this.catch401(event);
//          }  
//       });
//
//
//    /*   // extend server response observable with logging
//    return next.handle(req)
//      .pipe(
//        tap(
//          // Succeeds when there is a response; ignore other events
//          event => ok = event instanceof HttpResponse ? 'succeeded' : '',
//          // Operation failed; error is an HttpErrorResponse
//          error => ok = 'failed'
//        ),
//        // Log when response observable either completes or errors
//        finalize(() => {
//          const elapsed = Date.now() - started;
//          const msg = `${req.method} "${req.urlWithParams}"
//             ${ok} in ${elapsed} ms.`;
//          this.messenger.add(msg);
//        })
//      );*/
//  }
//// Request Interceptor to append Authorization Header
//  private setAuthorizationHeader(req: HttpRequest<any>): HttpRequest<any> {
//    // Make a clone of the request then append the Authorization Header
//    // Other way of writing :
//    // return req.clone({headers: req.headers.set('Authorization', this.authService.token )});
//    return req.clone({ setHeaders: { Authorization: this.authService.getToken() } });
//  }
//  // Response Interceptor
//  private catch401(error: HttpErrorResponse): Observable<any> {
//    // Check if we had 401 response
//    if (error.status === 401) {
//      // redirect to Login page for example
//      return Observable.empty();
//    }
//    return Observable.throw(error);
//  }
//}