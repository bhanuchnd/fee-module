import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import {Student} from '../../models/student';
import {Template} from '../../models/template/template';
import {Due} from '../../models/transaction/Due';
import {Mode} from '../../models/transaction/mode';
import {Transaction} from '../../models/transaction/transaction';
import {TransactionHistory} from '../../models/transaction/transactionHistory';
import {CommonService} from '../../service/common.service';
import {TokenService} from '../../service/token.service';
import {TransactionService} from '../../service/transaction.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  templateUrl: 'transaction.component.html',
  styleUrls: ['./transaction.component.css'],
  providers: [TransactionService, CommonService, ErrorsHandler, TokenService, AuthenticationService]
})
export class TransactionComponent implements OnInit {

  search: string;
  selected = false;
  created = false;
  cf;
  student: Student = new Student(-1,'',null,null,null,null,null);
  isModalOpen = false;
  modes: Mode[] = [];
  selectedStudent: any;
  today = new Date();
  transactionDashboard: any;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  studentTransaction: TransactionHistory = null;
  newTransaction: Transaction = null;
  typeaheadLoading = false;
  @ViewChild('ack') ack: ModalDirective;
  ackHead: string;
  ackMsg: string;
  typeaheadNoResults = false;
  isLoading = false;
  disabled = false;
  //  dataSource: Observable<any>;
  dataSource: Student[] = [];
  @ViewChild(ModalDirective) modal: ModalDirective;
  isNew: boolean;

  ngOnInit(): void {
    this.getTransactionDashboard();
    this.route.params.subscribe( params => {
      if (params.id) {
          this.selected = true;
          this.selectedStudent = params;
          this.getTransactionHistory(params);
      }

    });
    // if(this.student.id) {
    //   this.getTransactionHistory(this.student);
    // }
    this.cf = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      });
  }

  constructor(private transactionService: TransactionService, private modalService: BsModalService,
    private commonService: CommonService,
    public authenticationService: AuthenticationService,
    private errorHandler: ErrorsHandler,
    private tokenService: TokenService, private route: ActivatedRoute) {
    this.newTransaction = new Transaction();
    this.newTransaction.id = -1;
    this.newTransaction.template = new Template(-1, '', new Date(), Number(this.tokenService.getSchoolId()), -1, 0);
    this.newTransaction.due = 0;
    this.newTransaction.feeHeaderDefinitions = [];
    this.newTransaction.transaction_datetime = new Date();

  }

  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    if (this.created) {
      this.getTransactionHistory(this.selectedStudent);
    }
  }
  onValueChange(date: Date) {
    // let time = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
    // let onlyDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) +  '-' + ('0' + date.getDate()).slice(-2)
    // console.log(onlyDate + ' ' + time);
    // console.log(date);
  }
  getModes() {
    this.transactionService.getModes().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
      if (result) {
        this.modes = result.modes;
      } else {
        this.modes = [];
      }
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  getTransactionDashboard(): void {
    if (!this.ack.isShown) {
      this.isLoading = true;
    }
    this.transactionService.getDashboard().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.transactionDashboard = result.dashboardData;
      this.transactionDashboard.totalCollectionToday = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(result.dashboardData.totalCollectionToday);
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  getTransactionHistory(item): void {
    this.isLoading = true;
    this.transactionService.getTransactonHistory(item.id).subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (result) {
        this.studentTransaction = result
        // console.log(this.studentTransaction);
      } else {
        this.studentTransaction = null;
      }
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }

  public showModal(due: Due) {
    this.isModalOpen = true;
    this.getModes();
    this.newTransaction.id = due.id;
    this.newTransaction.template = due.template;
    this.newTransaction.due = due.due;
    this.newTransaction.paid = due.due;
    this.newTransaction.feeHeaderDefinitions = due.feeHeaderDefinitions || [];
    this.newTransaction.bankName = "";
    this.newTransaction.chequeNo = 0;
    this.newTransaction.chequeDate = "";
    this.newTransaction.creditCardName = "";
    this.newTransaction.creditCardNo = 0;
    this.newTransaction.debitCardNo = 0;
    this.newTransaction.debitCardName = "";
    this.newTransaction.student = this.studentTransaction.student;
    this.modal.show();

  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  changeTypeaheadNoResults(e: boolean): void {
    if (this.search !== '') {
      this.typeaheadLoading = true;
      this.commonService.getStudents({name: this.search}).subscribe((result) => {
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
        if (this.search.length > 0 && result && result.students.length <= 0) {
          this.typeaheadNoResults = true;
        }
        this.dataSource = result.students;
        // console.log(this.dataSource);
        this.typeaheadLoading = false;
      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ack.show();
        }
      });
    }
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.selected = true;
    this.selectedStudent = e.item;
    this.typeaheadNoResults = false;
    this.getTransactionHistory(e.item);
  }
  receive(studentTransaction: {}): void {
  }

  print(studentTransaction: {}): void {
  }
  receipt(transaction: Transaction) {
    this.isLoading = true;
    this.transactionService
    .printReceipt(transaction.id, transaction.template.id, this.studentTransaction.student.id, transaction.is_receipt)
    .subscribe((result) => {
      // if (result.session) {
      //   this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      // }
      // if (result.status === 200) {
      //   this.isLoading = false;
      //   if (result.invoce.length !== 0) {
      //     window.open(result.invoce, '_blank');
      //   } else {
      //     this.isLoading = false;
      //     this.ackHead = 'Not Found';
      //     this.ackMsg = 'No reports found';
      //     this.ack.show();
      //   }
      // }
      console.log(result);
      const mediaType = 'application/pdf';
      const blob = new Blob([result],  { type: 'application/pdf' });
      const pdf = URL.createObjectURL(blob);
      console.log(pdf);
      this.isLoading = false;
      window.open(pdf);
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  refresh() {
    this.modal.hide();
    this.newTransaction = new Transaction();
    this.newTransaction.id = -1;
    this.newTransaction.template = new Template(-1, '', new Date(), Number(this.tokenService.getSchoolId()), -1, 0);
    this.newTransaction.due = 0;
    this.newTransaction.feeHeaderDefinitions = [];
    this.newTransaction.transaction_datetime = new Date();
  }

  saveTransaction(transaction: Transaction): void {
    // this.isLoading = true;
    const date = transaction.transaction_datetime;
    let time = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
    let onlyDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) +  '-' + ('0' + date.getDate()).slice(-2);
    // console.log(onlyDate + ' ' + time);
    const newTransaction = transaction;
    newTransaction.transaction_datetime = onlyDate + ' ' +  time;
    this.isModalOpen = false;
    this.disabled = true;
    this.transactionService.createTransaction(newTransaction).subscribe((result: any) => {
      // this.isLoading = false;
      this.disabled = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (result.success) {
        this.created = true;
        this.ackHead = 'Payment Successfull';
        this.ackMsg = transaction.student.name + '\'s fee Payment of ' + this.cf.format(transaction.paid) + ' has been received';
        this.ack.show();
        this.refresh();
        this.getTransactionDashboard();
      } else {
        this.ackHead = 'Transaction Failed';
        this.ackMsg = transaction.student.name + '\'s fee Payment of ' + this.cf.format(transaction.paid) + ' has been failed. Please Try again';
        this.ack.show();
      }
      // this.modal.hide();
    }, error => {
      this.modal.hide();
      this.disabled = false;
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });

  }
}
