import { Class } from '../../models/class';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import {Template} from '../../models/template/template';
import {Classification} from '../../models/template/classification';
import { CommonService } from '../../service/common.service';

import { TemplateService } from '../../service/template.service';
import { TokenService } from '../../service/token.service';
import {BsDatepickerModule, ModalDirective} from 'ngx-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'ngx-dropdown-multiselect';
import 'rxjs/Rx' ;
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';
import { ErrorHandler } from '@angular/router/src/router';

@Component({
  templateUrl: 'reports.component.html',
  styleUrls: ['report.component.css'],
  providers: [TemplateService, ErrorsHandler, TokenService, CommonService, AuthenticationService]
})
export class ReportsComponent implements OnInit {

  reportTemplates: IMultiSelectOption[];
  fromDate;
  toDate;
  filePath;
  reportType = 1;
  typeOfReport;
  classfiy;
  template;
  reportData: any = null;
  options: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-light col-md-12 text-left',
    dynamicTitleMaxItems: 5,
    showCheckAll: true,
    closeOnClickOutside: true,
    showUncheckAll: true,
    closeOnSelect: true,
    containerClasses: 'd-none',
    displayAllSelectedText: true
};
  cf: any;
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
  };
  isLoading = false;
  selectedClass;
  studentData = [];
  classifications: IMultiSelectOption[] = null;
  classes: IMultiSelectOption[];
  @ViewChild('ack') ack: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  ackHead: string;
  ackMsg: string;
  classification: Classification = new Classification(-1, '', Number(this.tokenService.getSchoolId()), 0);
  constructor(private templateService: TemplateService,
     private  tokenService: TokenService,
     private errorHandler: ErrorsHandler,
    private commonService: CommonService, public authenticationService: AuthenticationService) {}

  ngOnInit(): void {
  this.fromDate = null;
  this.toDate = null;
  this.typeOfReport = null;
  this.classfiy = null;
  this.template = null;
  this.selectedClass = null;
  this.cf = new Intl.NumberFormat('en-IN', {
    style: 'currency',
    currency: 'INR',
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  });
  }
  selectClassifications(): void {
    if (this.typeOfReport !== 1) {
      this.getClassifications();
    } else {
      this.getClassifications();
      this.reportTemplates = null;
      this.classifications = null;
      this.classes = null;
    }
  }
  log(msg: string) {
    // console.log(msg);
  }
  reload() {
    window.location.reload();
  }
  // print() {
  //   // tslint:disable-next-line:prefer-const
  //   let printableDiv = document.getElementById('print_div').innerHTML;
  //   // tslint:disable-next-line:prefer-const
  //   let originalContents = document.body.innerHTML;
  //   document.body.innerHTML = printableDiv;
  //   window.print();
  //   document.body.innerHTML = originalContents;
  // }
  processData() {
    this.studentData = [];
    this.reportData.report_type = parseInt(this.reportData.report_type, 10);
    this.reportData.total_amount = this.cf.format(this.reportData.total_amount);
    Object.keys(this.reportData.result_report).forEach(studentId => {
      // tslint:disable-next-line:prefer-const
      let student = {};
      // tslint:disable-next-line:prefer-const
      let studentdata = this.reportData.result_report[studentId];
      if (this.reportData.report_type === 1) {
        studentdata.forEach(studentDetails => {
          if (student['studentId'] !== undefined && studentDetails.studentid === student['studentId']) {
            // tslint:disable-next-line:prefer-const
            let transaction = {};
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['date_time'] = studentDetails.date_time;
            student['transactions'].push(transaction);
          } else {
            this.studentData.push(student);
             // tslint:disable-next-line:prefer-const
            let transaction = {};
            student['transactions'] = [];
            student['studentId'] = studentDetails.studentid;
            student['name'] = studentDetails.name;
            student['class'] = studentDetails.class;
            student['section'] = studentDetails.section;
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['date_time'] = studentDetails.date_time;
            student['transactions'].push(transaction);
          }
        })
      } else if (this.reportData.report_type === 3 || this.reportData.report_type === 4 || this.reportData.report_type === 2) {
        studentdata.forEach(studentDetails => {
          if (student['studentId'] !== undefined && studentDetails.studentid === student['studentId']) {
            // tslint:disable-next-line:prefer-const
            let transaction = {};
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['dueAmount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.dueAmount);
            transaction['dueDate'] = studentDetails.dueDate;
            student['transactions'].push(transaction);
          } else {
            this.studentData.push(student);
             // tslint:disable-next-line:prefer-const
            let transaction = {};
            student['transactions'] = [];
            student['studentId'] = studentDetails.studentid;
            student['name'] = studentDetails.name;
            student['class'] = studentDetails.class;
            student['section'] = studentDetails.section;
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['dueAmount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.dueAmount);
            transaction['dueDate'] = studentDetails.dueDate;
            student['transactions'].push(transaction);
          }
        })
      } else if (this.reportData.report_type === 5) {
        studentdata.forEach(studentDetails => {
          if (student['studentId'] !== undefined && studentDetails.studentid === student['studentId']) {
            // tslint:disable-next-line:prefer-const
            let transaction = {};
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['actual_amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.actual_amount);
            transaction['discount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.discount);
            transaction['header'] = studentDetails.header;
            student['transactions'].push(transaction);
          } else {
            this.studentData.push(student);
             // tslint:disable-next-line:prefer-const
            let transaction = {};
            student['transactions'] = [];
            student['studentId'] = studentDetails.studentid;
            student['name'] = studentDetails.name;
            student['class'] = studentDetails.class;
            student['section'] = studentDetails.section;
            transaction['template'] = studentDetails.template;
            transaction['amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.amount);
            transaction['actual_amount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.actual_amount);
            transaction['discount'] = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(studentDetails.discount);
            transaction['header'] = studentDetails.header;
            student['transactions'].push(transaction);
          }
        })
      }
    })
  }
  getTemplateNames(): void {
    this.isLoading = true;
    this.templateService.getTemplates().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.reportTemplates = result.template;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });

  }
  selectClasses(): void {

    if (this.template.length === this.reportTemplates.length) {
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    }
    this.getClasses();
  }
  getClasses(): void {
    this.isLoading = true;
    this.commonService.getClasses().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (this.template.length === this.reportTemplates.length) {
        document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      }
     this.classes = result.classes;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  autoCloseOnSelectAll() {
    if (this.selectedClass.length === this.classes.length) {
      // console.log(this.selectedClass);
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      this.getSampleClasses();
    }
  }
  getSampleClasses() {
    this.commonService.getClasses().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
        document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    });
  }
  selectTemplates(): void {
    // console.log(this.classfiy);
    if (this.classfiy.length === this.classifications.length) {
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    }
    this.getTemplateNames();
  }
  getClassifications(): void {
    this.isLoading = true;
    this.templateService.getTemplateClassifications().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.classifications = result.classification;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }

  downloadReport(): void {
    // this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
    this.isLoading = true;
    this.commonService.downloadReport(this.typeOfReport, this.fromDate, this.toDate, this.template, this.classfiy, this.selectedClass).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      } else {
        if (result.status === '200') {
          this.isLoading = false;
          console.log(result.report.report_type);
          if (result.report.report_type) {
            this.reportData = result.report;
            this.processData();
          } else {
            this.ackHead = 'Nothing There';
            this.ackMsg = 'No reports found';
            this.ack.show();
            // this.authenticationService.isShown(this.sessionModal, this.ack);
          }
          // if (this.filePath.length !== 0) {
            // window.open(this.filePath, '_blank');
            // for html content
            // var report = window.open(this.filePath, '_blank');
            // report.document.write(result.html);
          // }
        } else {
          this.isLoading = false;
          this.ackHead = 'Failed';
          this.ackMsg = 'Failed while fetchting report';
          this.ack.show();
        }
      }

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
}
