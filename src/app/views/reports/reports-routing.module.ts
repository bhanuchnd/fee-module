import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';
import { AnalysisComponent } from './analysis/analysis.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: ReportsComponent,
  //   data: {
  //     title: 'Reports'
  //   }
  // },
  {
    path: '',
    component: AnalysisComponent,
    data: {
      title: 'ReportsReports'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {}
