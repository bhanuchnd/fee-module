import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from '@angular/common';


import { UiSwitchModule } from 'ngx-ui-switch';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule, ModalModule } from 'ngx-bootstrap';
import { MultiselectDropdownModule } from 'ngx-dropdown-multiselect';
import { LoadingModule } from 'ngx-loading';
import { AnalysisComponent } from './analysis/analysis.component';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ChartsModule,
    FormsModule,
    UiSwitchModule,
    LoadingModule,
    BsDropdownModule,
    MultiselectDropdownModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [ ReportsComponent, AnalysisComponent ]
})
export class ReportsModule { }
