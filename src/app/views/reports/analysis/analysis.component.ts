import { Class } from '../../../models/class';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import {Template} from '../../../models/template/template';
import {Classification} from '../../../models/template/classification';
import { CommonService } from '../../../service/common.service';

import { TemplateService } from '../../../service/template.service';
import { TokenService } from '../../../service/token.service';
import {BsDatepickerModule, ModalDirective} from 'ngx-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'ngx-dropdown-multiselect';
import 'rxjs/Rx' ;
import { AuthenticationService } from '../../../service/authentication.service';
import { AppComponent } from '../../../app.component';
import { ErrorsHandler } from '../../../errors-handler';
import { ErrorHandler } from '@angular/router/src/router';
import {reportType} from '../../../app.global';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
declare var jsPDF: any;

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss'],
  providers: [TemplateService, ErrorsHandler, TokenService, CommonService, AuthenticationService]
})
export class AnalysisComponent implements OnInit {

  reportTemplates: IMultiSelectOption[];
  fromDate;
  toDate;
  filePath;
  headerCount = 4;
  reportType = reportType;
  typeOfReport;
  classfiy;
  template;
  isGrouped = 0;
  currentDate = new Date().toISOString().slice(0, 10);
  classificationObjects = {};
  reportData: any = null;
  options: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-light col-md-12 text-left',
    dynamicTitleMaxItems: 5,
    showCheckAll: true,
    closeOnClickOutside: true,
    showUncheckAll: true,
    closeOnSelect: true,
    containerClasses: 'd-none',
    displayAllSelectedText: true
};
  cf: any;
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
  };
  isLoading = false;
  selectedClass;
  studentData = [];
  classifications: IMultiSelectOption[] = null;
  classes: IMultiSelectOption[];
  classObjects = {};
  classNames = '';
  classificationNames = '';
  @ViewChild('ack') ack: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  ackHead: string;
  ackMsg: string;
  classification: Classification = new Classification(-1, '', Number(this.tokenService.getSchoolId()), 0);
  constructor(private templateService: TemplateService,
     private  tokenService: TokenService,
     private errorHandler: ErrorsHandler,
    private commonService: CommonService, public authenticationService: AuthenticationService) {}

  ngOnInit(): void {
  this.fromDate = null;
  this.toDate = null;
  this.typeOfReport = null;
  this.classfiy = null;
  this.template = null;
  this.selectedClass = null;
  this.cf = new Intl.NumberFormat('en-IN', {});
  }
  selectClassifications(): void {
    this.classfiy = null;
    // this.selectedClass = [];
    if (this.typeOfReport.value !== 1) {
      this.getClassifications();
    } else {
      this.getClassifications();
      this.classfiy = null;
      this.reportTemplates = null;
      this.classifications = null;
      this.classes = [];
    }
  }
  log(msg: string) {
    // console.log(msg);
  }
  reload() {
    window.location.reload();
  }
  // print() {
  //   // tslint:disable-next-line:prefer-const
  //   let printableDiv = document.getElementById('print_div').innerHTML;
  //   // tslint:disable-next-line:prefer-const
  //   let originalContents = document.body.innerHTML;
  //   document.body.innerHTML = printableDiv;
  //   window.print();
  //   document.body.innerHTML = originalContents;
  // }
  checked() {
    if (this.isGrouped) {
      this.isGrouped = 0;
    } else {
      this.isGrouped = 1;
    }
    console.log(this.isGrouped);
  }
  getTemplateNames(): void {
    this.isLoading = true;
    this.templateService.getTemplates().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.reportTemplates = result.template;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });

  }
  selectClasses(): void {
    this.selectedClass = null;
    if (this.classfiy.length === this.classifications.length) {
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    }
    this.getClasses();
  }
  getClasses(): void {
    this.isLoading = true;
    this.commonService.getClasses().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (this.classfiy.length === this.classifications.length) {
        document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      }
     this.classes = result.classes;
     this.classes.forEach(element => {
       this.classObjects[element.id] = element.name;
     })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  autoCloseOnSelectAll() {
    if (this.selectedClass.length === this.classes.length) {
      // console.log(this.selectedClass);
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
      this.getSampleClasses();
    }
  }
  getSampleClasses() {
    this.commonService.getClasses().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
        document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    });
  }
  selectTemplates(): void {
    // console.log(this.classfiy);
    if (this.classfiy.length === this.classifications.length) {
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    }
    this.getTemplateNames();
  }
  getClassifications(): void {
    this.isLoading = true;
    this.templateService.getTemplateClassifications().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.classifications = result.classification;
      this.classifications.forEach(element => {
        this.classificationObjects[element.id] = element.name;
      })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }

  downloadReport(): void {
    // this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
    this.isLoading = true;
    this.commonService.downloadNewReport(this.typeOfReport.value, this.fromDate,
       this.toDate, this.template, this.classfiy, this.selectedClass, this.isGrouped).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      } else {
        if (result.status === '200') {
          this.isLoading = false;
          if (result.report.length > 0) {
            this.setHeaderCount();
            this.reportData = result;
            this.classNames = '';
            this.classificationNames = '';
            if (this.selectedClass.length !== this.classes.length) {
              for (let i = 0; i < this.selectedClass.length; i++) {
                this.classNames += this.classObjects[this.selectedClass[i]];
                this.classNames += this.selectedClass.length > 0 ? ',' : '';
              }
            } else {
              this.classNames = 'All Classes';
            }
            if (this.classfiy.length !== this.classifications.length) {
              for (let i = 0; i < this.classfiy.length; i++) {
                this.classificationNames += this.classificationObjects[this.classfiy[i]];
                this.classificationNames += this.classfiy.length > 0 ? ',' : '';
              }
            } else {
              this.classificationNames = 'All Terms';
            }
          } else {
            this.ackHead = 'Nothing There';
            this.ackMsg = 'No reports found';
            this.ack.show();
          }
        } else {
          this.isLoading = false;
          this.ackHead = 'Failed';
          this.ackMsg = 'Failed while fetchting report';
          this.ack.show();
        }
      }

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  setHeaderCount() {
    if (this.typeOfReport.value === 1) {
      this.headerCount = !this.isGrouped ? 4 : 4;
    } else if (this.typeOfReport.value === 2 || this.typeOfReport.value === 4) {
      this.headerCount = !this.isGrouped ? 5 : 4;
    } else if (this.typeOfReport.value === 3) {
      this.headerCount = !this.isGrouped ? 6 : 4;
    } else if (this.typeOfReport.value === 5) {
      this.headerCount = !this.isGrouped ? 6 : 6;
    }
  }
  getColumns() {
    let columns = [
      {title: 'Student Name', dataKey: 'name'},
      {title: 'Section', dataKey: 'section'},
      {title: 'Amount', dataKey: 'amount'},
      {title: 'Classifications', dataKey: 'classification_name'},
    ];
    if (this.typeOfReport.value === 1) {
      return columns;
    } else if (this.typeOfReport.value === 2 || this.typeOfReport.value === 4) {
      columns = [
        {title: 'Student Name', dataKey: 'name'},
        {title: 'Section', dataKey: 'section'},
        {title: 'Amount', dataKey: 'amount'},
        {title: 'Classifications', dataKey: 'classification_name'},
        {title: 'Due Date', dataKey: 'dueDate'},
        {title: 'Overdue Days', dataKey: 'overdue_days'},
      ];
    } else if (this.typeOfReport.value === 3) {
      columns = [
        {title: 'Student Name', dataKey: 'name'},
        {title: 'Section', dataKey: 'section'},
        {title: 'Amount', dataKey: 'amount'},
        {title: 'Classifications', dataKey: 'classification_name'},
        {title: 'Due Date', dataKey: 'dueDate'},
        {title: 'Overdue Days', dataKey: 'overdue_days'},
      ];
      if (this.isGrouped) {
        columns = [
          {title: 'Student Name', dataKey: 'name'},
          {title: 'Section', dataKey: 'section'},
          {title: 'Amount', dataKey: 'amount'},
          {title: 'Classifications', dataKey: 'classification_name'},
        ];
      }
    } else if (this.typeOfReport.value === 5) {
      columns = [
        {title: 'Student Name', dataKey: 'name'},
        {title: 'Section', dataKey: 'section'},
        {title: 'Amount', dataKey: 'amount'},
        {title: 'Classifications', dataKey: 'classification_name'},
        {title: 'Due Date', dataKey: 'dueDate'},
        {title: 'Overdue Days', dataKey: 'overdue_days'},
      ];
    }
    return columns;
  }
  downloadPdf() {
    const columns = this.getColumns();
    const doc = new jsPDF('p', 'pt');
    const classTables = document.getElementsByClassName('table');
    const classHeader = document.getElementsByClassName('class_overview');
    let count = 0;
    this.reportData.report.forEach(classes => {
     const body = [['Class: ' + this.classNames, 'Classifications: ' + this.classificationNames], [this.isGrouped ? 'Grouped' : 'Un-Grouped', ]];
      const rows = classes.student_list;
      if (count === 0) {
        doc.autoTable({
          theme: 'plain',
          bodyStyles: {
            halign: 'center',
            fontSize: 16,
            fontStyle: 'bold'
          },
          body: [[this.typeOfReport.text]]
        })
        doc.autoTable({
          theme: 'plain',
          html: '#report_info',
          didParseCell : function (row) {
            if (row.column.dataKey === '1') {
              row.cell.styles.halign = 'right'
            }
          },
          startY: doc.previousAutoTable.finalY,
        });
        doc.autoTable({
          html: '#class_overview',
          footStyles: {
            fillColor: [255, 255, 255],
            textColor: [0, 0, 0],
          },
          didParseCell : function (row) {
            if (row.column.dataKey === '1') {
              row.cell.styles.halign = 'right'
            }
          },
          // addPageContent: pageContent,
          startY: doc.previousAutoTable.finalY,
        });
        doc.autoTable({
          html: '#table_' + count,
          showHeader: 'firstPage',
          didParseCell : function (row) {
            if (row.cell.text[0].match(/Class:.*/) && row.section === 'head') {
              row.cell.styles.fillColor = [77, 189, 116];
            }
            if (row.cell.text[0] === '' && row.section === 'head') {
              row.cell.styles.fillColor = [77, 189, 116];
            }
            if (row.cell.text[0].match(/Total:.*/) && row.section === 'head') {
              row.cell.styles.fillColor = [77, 189, 116];
            }
          },
          startY: doc.previousAutoTable.finalY + 10
        });
      } else {
doc.autoTable({
  html: '#table_' + count,
  showHeader: 'firstPage',
  didParseCell : function (row) {
    if (row.cell.text[0].match(/Class:.*/) && row.section === 'head') {
      row.cell.styles.fillColor = [77, 189, 116];
    }
    if (row.cell.text[0] === '' && row.section === 'head') {
      row.cell.styles.fillColor = [77, 189, 116];
    }
    if (row.cell.text[0].match(/Total:.*/) && row.section === 'head') {
      row.cell.styles.fillColor = [77, 189, 116];
    }
  },
  startY: doc.previousAutoTable.finalY + 10
});
      }
      count++;
    });
    doc.save(this.typeOfReport.text + this.currentDate + ' .pdf');
    }

}
