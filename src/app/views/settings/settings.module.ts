import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { MultiselectDropdownModule } from 'ngx-dropdown-multiselect';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
    ModalModule.forRoot(),
    SettingsRoutingModule,
    MultiselectDropdownModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [SettingsComponent]
})
export class SettingsModule { }
