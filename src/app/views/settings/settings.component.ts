import { Component, OnInit, ViewChild } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'ngx-dropdown-multiselect';
import { CommonService } from '../../service/common.service';
import { AuthenticationService } from '../../service/authentication.service';
import { ModalDirective } from 'ngx-bootstrap';
import { TokenService } from '../../service/token.service';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [ErrorsHandler, CommonService, AuthenticationService, TokenService]
})
export class SettingsComponent implements OnInit {

  options: IMultiSelectOption[];
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  @ViewChild('ack') ack: ModalDirective;
  ackHead: string;
  ackMsg: string;
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-light col-md-12 text-left',
    dynamicTitleMaxItems: 5,
    showCheckAll: true,
    closeOnClickOutside: true,
    showUncheckAll: true,
    containerClasses: 'd-none',
    displayAllSelectedText: true
  };
  id: number;
  menuItems: number[] = [];
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
  };
  constructor(private commonService: CommonService,
    private errorHandler: ErrorsHandler,
     public authenticationService: AuthenticationService, private tokenService: TokenService) { }

  ngOnInit() {
    this.getMenuItems();
  }
  getMenuItems() {
    this.commonService.getMenuItems().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.id = result.id;
      this.options = result.menus;
      result.menus.forEach(element => {
        if (element.isAssigned) {
          this.menuItems.push(element.id);
        }
      })
      // console.log(this.menuItems);
    }, error => {
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  mapMenuItems() {
    if (this.menuItems.length > 0) {
      this.commonService.mapMenuItems(this.menuItems.join(), this.id, this.tokenService.getSchoolId())
      .subscribe((result: any) => {
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
        if (result.success) {
          this.ackHead = 'Mapped';
          this.ackMsg = 'Selected Menu Items are Mapped';
          this.ack.show();
        } else {
          this.ackHead = 'Failed';
          this.ackMsg = 'Selected Menu Items are Failed. Please try again';
          this.ack.show();
        }
      }, error => {
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ack.show();
        }
      });
    } else {
      this.ackHead = 'Nothing Selected';
      this.ackMsg = 'Please select atleast one Menu Item to map';
      this.ack.show();
    }
  }

}
