import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from "@angular/common";

import {ModalModule} from 'ngx-bootstrap';

import {FormsModule} from '@angular/forms';


import {FeeStructureComponent} from './fee-structure';

import { LoadingModule } from 'ngx-loading';
import { MultiselectDropdownModule } from 'ngx-dropdown-multiselect';
// Components Routing
import {MappingRoutingModule} from './mapping-routing.module';
import { StudentWiseComponent } from './student-wise.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    MappingRoutingModule,
    MultiselectDropdownModule,
    //  BsDropdownModule.forRoot(),
    ModalModule.forRoot(),

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [
    FeeStructureComponent,
    StudentWiseComponent

  ]
})
export class MappingModule {}
