import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {FeeHeader} from '../../models/template/feeHeader';
import {Section} from '../../models/section';
import {Student} from '../../models/student';
import {TemplateService} from '../../service/template.service';
import {StudentService} from '../../service/student.service';
import {CommonService} from '../../service/common.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {HeaderDefinition} from '../../models/template/headerDefinition';
import {Class} from '../../models/class';
import {Template} from '../../models/template/template';
import {FeeStructure} from '../../models/template/feeStructure';
import {AuthenticationService} from '../../service/authentication.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'ngx-dropdown-multiselect';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';
@Component({
  templateUrl: 'fee-structure.component.html',
  selector: 'app-feestructure-list',
    styleUrls: ['./fee-structure.component.css'],
  providers: [ErrorsHandler, TemplateService, StudentService, CommonService, AuthenticationService]
})
export class FeeStructureComponent implements OnInit {
  feeHeaderDefinition: HeaderDefinition = new HeaderDefinition(null, 0);
  feeHeaders: FeeHeader[] = [];
  feeTemplates = {};
  templateObjects = {};
  classObjects = {};
  headerSelector = {};
  isLoading = false;
  classes: Class[] = [];
  disabled = false;
  templates: Template[] = [];
 // selectedStructure = 0;
  ackHead: string;
  selectedSection: any;
  selectedStudents: number[] = [];
  ackMsg: string;
  feeStructure: FeeStructure = new FeeStructure(-1, null,null, null, [], null, 0, 0, 1);
  feeStructures: FeeStructure[] = [];
  created = false;
  title = 'Add Fee Structure';
  options: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    enableSearch: true,
    buttonClasses: 'btn btn-light col-md-12 text-left',
    dynamicTitleMaxItems: 2,
    showCheckAll: true,
    closeOnClickOutside: true,
    showUncheckAll: true,
    displayAllSelectedText: true
};
myTexts: IMultiSelectTexts = {
  checkAll: 'Select all',
  uncheckAll: 'Unselect all',
  checked: 'item selected',
  searchPlaceholder: 'Search Students',
  searchEmptyResult: 'Student not found',
};
  sections: Section[];
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('ack') ack: ModalDirective;
  @ViewChild('viewStudents') viewStudents: ModalDirective;
  @ViewChild('confirm') confirm: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  isNew: boolean;
  constructor(private templateService: TemplateService,
     private studentService: StudentService, private commonService: CommonService,
    private modalService: BsModalService, private errorHandler: ErrorsHandler,
    public authenticationService: AuthenticationService) {}



  ngOnInit() {
    this.getFeeHeaders();
    this.getClasses();
    this.getTemplates();
    this.getFeeStructures();

  }

  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    // refresh the list
    if (this.created) {
      this.getFeeStructures();
    }
  }
  getFeeStructures(): void {
    this.isLoading = true;
    this.templateService.getFeeStructures().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
      this.feeStructures = result.fs;
      this.feeStructures.forEach(element => {
        this.feeTemplates[element.template.id] = element;
      })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  // getSections(): void {
  //   if(this.selectedStudents !== undefined && this.selectedStudents.length > 0) {
  //     console.log(this.selectedStudents);
  //   }
  //   this.commonService.getSections(this.feeStructure.class.id).subscribe((result: any) => {
  //     this.sections = result.sections;
  //   })
  // }

  // getStudents(): void {
  //   this.studentService.viewStudents(this.feeStructure.class.id, this.selectedSection.id).subscribe((result: any) => {
  //     this.options = result.students;
  //   })
  // }
  getFeeHeaders(): void {
    this.templateService.getTemplateFeeHeaders().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      } else {
        this.feeHeaders = result.headers;
        result.headers.forEach(element => {
          this.headerSelector[element.id] = false;
        })
      }
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  getClasses(): void {
    this.commonService.getClasses().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
      this.classes = result.classes;
      this.classes.forEach(element => {
        this.classObjects[element.id] = element;
      })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }

  getTemplates(): void {
    this.templateService.getTemplates().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
      this.templates = result.template;
      this.templates.forEach(element => {
        this.templateObjects[element.id] = element;
      })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });

  }
  addDefinition() {
    this.feeStructure.total += this.feeHeaderDefinition.amount;
    this.feeStructure.feeHeaderDefinitions.push(this.feeHeaderDefinition)
    this.headerSelector[this.feeHeaderDefinition.feeHeader.id] = true;
    this.feeHeaderDefinition = new HeaderDefinition(null, 0);
  }

  deleteDefinition(index) {
    this.feeStructure.total -= this.feeStructure.feeHeaderDefinitions[index].amount;
    this.headerSelector[this.feeStructure.feeHeaderDefinitions[index].feeHeader.id] = false;
    this.feeStructure.feeHeaderDefinitions.splice(index, 1);
  }

  saveFeeStructure() {
    if (this.feeStructure.is_class_template === 1) {
      this.selectedStudents = [];
    }
    this.disabled = true;
    if (this.feeStructure.id > 0) {
      this.feeStructure.student_ids = this.selectedStudents;
      this.templateService.updateFeeStructure(this.feeStructure).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
        if (result.success) {
          this.getFeeStructures();
        this.ackHead = 'Info';
        this.ackMsg = 'Fee-Structure has been updated for Class-' + this.feeStructure.class.name;
        this.ack.show();
        this.modal.hide();
        }
      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ack.show();
        }
      });
    } else {
      this.feeStructure.student_ids = this.selectedStudents;
      this.templateService.createFeeStructure(this.feeStructure).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
        if (result.success) {
        this.getFeeStructures();
        this.ackHead = 'Info';
        this.ackMsg = 'Fee-Structure has been created for Class-' + this.feeStructure.class.name;
        this.ack.show();
        this.modal.hide();
        }
      }, error => {
        this.disabled = false;
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ack.show();
        }
      });
    }
    this.refresh();
  }
  refresh() {
  this.feeStructure = new FeeStructure(-1, null, null, null , [], null, 0, 0, 1);
    this.modal.hide();
    // this.getFeeHeaders();
    // this.getClasses();
    // this.getTemplates();
  }
  checkTemplate(event) {
    if (this.feeTemplates[this.feeStructure.template.id] !== null && this.feeTemplates[this.feeStructure.template.id] !== undefined) {
      this.feeStructure = this.feeTemplates[this.feeStructure.template.id];
      this.feeStructure.template = this.templateObjects[this.feeStructure.template.id];
      this.feeStructure.class = this.classObjects[this.feeStructure.class.id];
    } else {
      this.feeStructure = new FeeStructure(-1, this.templateObjects[this.feeStructure.template.id],null, null, [], null, 0, 0, 1);
    }
  }
  setFeeStructure(feeStructure: FeeStructure): void {
    this.feeStructure = feeStructure;
    this.title = 'Delete';
    this.ackMsg = 'Are you sure want to delete?';
    this.ackHead = 'Delete';
    this.confirm.show();
  }
  public delete(feeStructure: FeeStructure) {
    this.templateService.deleteFeeStructure(feeStructure.id).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.refresh();
      this.getFeeStructures();
      this.feeTemplates[feeStructure.template.id] = undefined;
      this.confirm.hide();
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  public showModal(isNew, feeStructure: FeeStructure) {
    if (isNew) {
      this.feeStructure = new FeeStructure(-1, null, null, null , [], null, 0, 0, 1);
      this.feeHeaderDefinition = new HeaderDefinition(null, 0);
      this.selectedStudents = [];
      this.headerSelector = {};
      this.modal.show();
    } else {
      this.title = 'Edit Fee Structure';
      feeStructure.feeHeaderDefinitions.forEach(element => {
          this.headerSelector[element.feeHeader.id] = true;
        })
      this.feeStructure = feeStructure;
      this.feeStructure.template = this.templateObjects[this.feeStructure.template.id];
      this.feeStructure.class = this.classObjects[this.feeStructure.class.id];
      if (this.feeStructure.is_class_template === 0) {
        this.feeStructure.students.forEach((element,index) => {
          this.selectedStudents[index] = element.id;
          console.log(element);
        })
        // console.log(this.selectedStudents);
      }
      // this.getSections();
      this.modal.show();
    }
  }
}
