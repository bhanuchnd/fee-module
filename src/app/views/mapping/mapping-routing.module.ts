import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { FeeStructureComponent } from './fee-structure';
import { StudentWiseComponent } from './student-wise.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Mapping'
    },
    children: [
      {
        path: 'fee-structure',
        component: FeeStructureComponent,
        data: {
          title: 'Map Fee Structure'
        }
      },
      {
        path: 'student-fee-structure',
        component: StudentWiseComponent,
        data: {
          title: 'Map Student-Wise Fee Structure'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MappingRoutingModule {}
