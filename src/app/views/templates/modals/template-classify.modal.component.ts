import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { Classification } from '../../../models/template/classification';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TemplateService } from '../../../service/template.service';
/* This is a component which we pass in modal*/

@Component( {
    selector: 'app-modal-template',
    templateUrl: 'template-classify.modal.component.html',
    providers: [TemplateService]
} )
export class ModalClassifyComponent implements OnInit {
    public title: string;

    @Input()
    public classification: Classification;
    @Output() onCreated = new EventEmitter<Classification>();
    created = false;
    constructor( public bsModalRef: BsModalRef, private templateService: TemplateService ) {
    }
    //  classification:Classification;



    ngOnInit() {

    }
    handler(modalStatus, event) {

    }
    saveClassification() {
        if ( this.classification.id > 0 ) {
            this.templateService.updateTemplateClassification( this.classification ).subscribe(( classification: Classification ) => {

                // newly created
                this.onCreated.emit( classification );
                this.created = true;
                this.bsModalRef.hide();

              });
            }
          else {
              this.templateService.createTemplateClassification( this.classification ).subscribe(( classification: Classification ) => {

                  //newly created
                  this.onCreated.emit( classification );
                  this.created = true;
                  this.bsModalRef.hide();

                });
      }
         
    }
}
