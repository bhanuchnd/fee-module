import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {TemplateClassificationComponent} from './template-classify';
import {TemplateFeeHeadersComponent} from './template-headers';
import {TemplateNameComponent} from './template-name';
import {ModalClassifyComponent} from './modals/template-classify.modal.component'

import {BsDatepickerModule} from 'ngx-bootstrap';

import { LoadingModule } from 'ngx-loading';
// Components Routing
import {TemplatesRoutingModule} from './templates-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    TemplatesRoutingModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot()

    //   BsDropdownModule.forRoot(),
    // ModalModule.forRoot,
  ],
  declarations: [
    TemplateClassificationComponent,
    ModalClassifyComponent,
    TemplateFeeHeadersComponent,
    TemplateNameComponent
  ],
  entryComponents: [ModalClassifyComponent]
})
export class TemplatesModule {}
