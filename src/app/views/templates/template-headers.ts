import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {FeeHeader} from '../../models/template/feeHeader';
import {AuthenticationService} from '../../service/authentication.service';
import {TemplateService} from '../../service/template.service';
import {TokenService} from '../../service/token.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalClassifyComponent} from './modals/template-classify.modal.component'
import {ModalDirective} from 'ngx-bootstrap/modal';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  templateUrl: 'template-headers.component.html',
  selector: 'app-fee-header-list',
  providers: [ErrorsHandler, TemplateService, AuthenticationService, TokenService],
    styleUrls: ['./template-headers.component.css']
})
export class TemplateFeeHeadersComponent implements OnInit {

  headers: FeeHeader[];
  created = false;
  isLoading = false;
  disabled = false;
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('ack') ackModal: ModalDirective;
  @ViewChild('confirm') confirm: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  isNew: boolean;
  title = 'Add Fee Header';
  header: FeeHeader = new FeeHeader(-1, '', Number(this.tokenService.getSchoolId()), 0);
  constructor(private templateService: TemplateService, private modalService: BsModalService,
    public authenticationService: AuthenticationService,
    private errorHandler: ErrorsHandler,
    private tokenService: TokenService) {}

  ngOnInit() {
    this.getFeeHeaders();

  }



  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    //  refresh the list
    if (this.created) {
      this.getFeeHeaders();
    }


  }

  getFeeHeaders(): void {
    this.isLoading = true;
    this.templateService.getTemplateFeeHeaders().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      } else {
        this.headers = result.headers;
        // console.log(this.headers.length);
      }

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });

  }
  public saveFeeHeader() {
    this.disabled = true;
    if (this.header.id > 0) {
      this.templateService.updateTemplateFeeHeader(this.header).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
        if (result.success) {
          this.ackHead = 'Info';
          this.created = false;
          this.ackMsg = 'Fee header has been updated';
          this.modal.hide();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed while updating Fee header. Please try again!';
          this.modal.hide();
          this.ackModal.show();
        }

      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    } else {
      this.templateService.createTemplateFeeHeader(this.header).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
        if (result.success) {
          this.created = true;
          this.ackHead = 'Info';
          this.ackMsg = 'Fee Header has been created';
          this.modal.hide();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed while creating fee header. Please try again!';
          this.modal.hide();
          this.ackModal.show();
        }
      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    }

  }

  public delete(header: FeeHeader) {

    this.templateService.deleteTemplateFeeHeader(header.id).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
      this.getFeeHeaders();
    this.confirm.hide();

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }
  setHeader(header: FeeHeader): void {
    this.header = header;
    // console.log(this.header);
    this.ackMsg = 'Are you sure want to delete?';
    this.confirm.show();
  }

  public showModal(isNew, header: FeeHeader) {
    if (isNew) {
      this.header = new FeeHeader(-1, '', Number(this.tokenService.getSchoolId()), 0);
      this.modal.show();
    } else {
      this.title = 'Edit Fee Header';
      this.header = header;
      this.modal.show();
    }



  }
  public(type: string, $event: ModalDirective) {


  }
}
