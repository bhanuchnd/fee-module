import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemplateClassificationComponent } from './template-classify';
import {  TemplateFeeHeadersComponent} from './template-headers';
import { TemplateNameComponent } from './template-name';




const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Templates'
    },
    children: [
      {
        path: 'template-name',
        component: TemplateNameComponent,
        data: {
          title: 'Template Name'
        }
      },
      {
        path: 'template-headers',
        component: TemplateFeeHeadersComponent,
        data: {
          title: 'Template Headers'
        }
      },
      {
        path: 'template-classify',
        component: TemplateClassificationComponent,
        data: {
          title: 'Template Classify'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplatesRoutingModule {}
