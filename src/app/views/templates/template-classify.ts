import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Classification} from '../../models/template/classification';
import {TemplateService} from '../../service/template.service';
import {TokenService} from '../../service/token.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalClassifyComponent} from './modals/template-classify.modal.component'
import {ModalDirective} from 'ngx-bootstrap/modal';
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorHandler } from '@angular/router/src/router';
import { ErrorsHandler } from '../../errors-handler';


@Component({
    templateUrl: 'template-classify.component.html',
    selector: 'app-classify-list',
    providers: [ErrorsHandler, TemplateService, TokenService, AuthenticationService],
    styleUrls: ['./template-classify.component.css']
})
export class TemplateClassificationComponent implements OnInit {

  classifications: Classification[];
  created = false;
  isLoading = false;
  @ViewChild('addModal') addModal: ModalDirective;
  @ViewChild('confirm') confirm: ModalDirective;
  isNew: boolean;
  @ViewChild('ack') ackModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  disabled = false;
  title = 'Add Classification';
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  classification: Classification = new Classification(-1, '', Number(this.tokenService.getSchoolId()), 0);
  constructor(private templateService: TemplateService,
     private tokenService: TokenService, private modalService: BsModalService,
      public authenticationService: AuthenticationService, private errorHandler: ErrorsHandler) {}

  ngOnInit() {
    this.getClassifications();

  }



  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    // refresh the list
    if (this.created) {
      this.getClassifications();
    }


  }

  getClassifications(): void {
    this.isLoading = true;
    this.templateService.getTemplateClassifications().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.classifications = result.classification;
      // console.log(this.classifications);
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });

  }
  public saveClassification() {
    this.disabled = true;
    if (this.classification.id > 0) {
      this.templateService.updateTemplateClassification(this.classification).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
        if (result.success) {
          this.ackHead = 'Info';
          this.created = false;
          this.ackMsg = 'Classification name has been updated';
          this.addModal.hide();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed while updating Classification name . Please try again!';
          this.addModal.hide();
          this.ackModal.show();
        }

      }, error => {
        this.addModal.hide();
        this.disabled = false;
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    } else {
      this.templateService.createTemplateClassification(this.classification).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
        if (result.success) {
          this.created = true;
          this.ackHead = 'Info';
          this.ackMsg = 'Classification has been created';
          this.addModal.hide();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed whiled Creating Classification. Please try again!';
          this.addModal.hide();
          this.ackModal.show();
        }
      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    }

  }

  setClassification(classification: Classification) {
    this.classification = classification;
    this.ackMsg = 'Are you sure want to delete?';
    this.confirm.show();

  }
  public delete(classification: Classification) {
    this.templateService.deleteTemplateClassification(classification.id).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.getClassifications();
      this.confirm.hide();
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }

  public showModal(isNew, classification: Classification) {
    if (isNew) {
      this.classification = new Classification(-1, '', Number(this.tokenService.getSchoolId()), 0);
      this.addModal.show();
    } else {
      this.title = 'Edit Classification';
      this.classification = classification;
      this.addModal.show();
    }



  }
  public(type: string, $event: ModalDirective) {


  }
}
