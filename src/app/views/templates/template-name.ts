import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Template} from '../../models/template/template';
import {TemplateService} from '../../service/template.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalClassifyComponent} from './modals/template-classify.modal.component'
import {ModalDirective} from 'ngx-bootstrap/modal';
import {Classification} from '../../models/template/classification';
import {TokenService} from '../../service/token.service';
import {AuthenticationService} from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';
@Component({
  templateUrl: 'template-name.component.html',
  selector: 'app-template-list',
  providers: [TemplateService, TokenService, ErrorsHandler, AuthenticationService],
    styleUrls: ['./template-name.component.css']
})
export class TemplateNameComponent {

  classifications: Classification[];
  templates: Template[];
  isLoading = false;
  created?: boolean = false;
  title = 'Add Template';
  disabled = false;
  minDate = new Date();
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('confirm') confirm: ModalDirective;
  @ViewChild('confirmSave') confirmSave: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  isNew: boolean;
  @ViewChild('ack') ackModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  template: Template = new Template(-1, '', null, Number(this.tokenService.getSchoolId()), -1, 0);
  constructor(private templateService: TemplateService,
    private tokenService: TokenService,
    public authenticationService: AuthenticationService,
    private errorHandler: ErrorsHandler,
    private modalService: BsModalService) {}

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.getClassifications();
    this.getTemplates();

  }



  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    // refresh the list
    if (this.created) {
      this.getTemplates();
    }


  }

  getClassifications(): void {

    this.templateService.getTemplateClassifications().subscribe((result: any) => {
    if (result.session) {
      this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
    }
      this.classifications = result.classification;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });

  }

  getTemplates(): void {
    this.isLoading = true;
    this.templateService.getTemplates().subscribe((result:any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
      }
    this.templates = result.template;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });

  }
  public saveTemplate() {
    this.disabled = true;
    this.confirmSave.hide();
    if (this.template.id > 0) {
      this.templateService.updateTemplate(this.template).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
         if (result.success) {
          this.ackHead = 'Template Updated';
          this.created = false;
          this.ackMsg = 'SMS will be triggered automatically based on this over due date ' + this.template.overdueDate;
          this.modal.hide();
          this.getTemplates();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed while updating Template name . Please try again!';
          this.modal.hide();
          this.ackModal.show();
        }

      }, error => {
        this.isLoading = false;
        this.disabled = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    } else {
      this.templateService.createTemplate(this.template).subscribe((result: any) => {
        this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
         if (result.success) {
          this.ackHead = 'Template Created';
          this.created = false;
          this.ackMsg = 'SMS will be triggered automatically based on this over due date ' + this.template.overdueDate;
          this.modal.hide();
          this.getTemplates();
          this.ackModal.show();
        } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Failed while updating Template name . Please try again!';
          this.modal.hide();
          this.ackModal.show();
        }

      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    }

  }
  setTemplate(template: Template) {
    this.template = template;
    this.ackMsg = 'Are you sure want to delete?';
    this.confirm.show();
  }
  confirmBeforeSave() {
    this.modal.hide();
    this.ackMsg = 'SMS will be triggered automatically based on over due Date. Are you want to Continue?';
    this.confirmSave.show();
  }
  public delete(template: Template) {
      this.templateService.deleteTemplate(template.id).subscribe((result: any) => {
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal,this.ackHead,this.ackMsg);
        }
      this.getTemplates();
      this.confirm.hide();

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }
  public showModal(isNew, template: Template) {
    if (isNew) {
      this.template = new Template(-1, '', null, Number(this.tokenService.getSchoolId()), -1, 0);
      this.modal.show();
    } else {
      this.title = 'Edit Template';
      this.template = template;
      this.modal.show();
    }

  }
  public(type: string, $event: ModalDirective) {


  }

}
