import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {TypeaheadModule} from 'ngx-bootstrap';
import {BsDatepickerModule} from 'ngx-bootstrap';
import { LoadingModule } from 'ngx-loading';
import {LoginComponent} from './login.component';
import {LoginRoutingModule} from './login-routing.module';
import { P404Component } from './404.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    ChartsModule,
    FormsModule,
    BsDropdownModule,
    LoadingModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
   schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [LoginComponent, P404Component]
})
export class LoginModule {}
