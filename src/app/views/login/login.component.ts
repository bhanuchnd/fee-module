import {Component, ViewChild, OnInit} from '@angular/core';
import {Login} from '../../models/login';
import {AuthenticationService} from '../../service/authentication.service';
import {CommonService} from '../../service/common.service';
import {TokenService} from '../../service/token.service';
import {Router, CanActivate, ActivatedRoute} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {AppHeaderComponent} from '../../components/app-header/app-header.component';
import { Location } from '@angular/common';
import { ErrorsHandler } from '../../errors-handler';
@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [ErrorsHandler, TokenService]
})
export class LoginComponent implements OnInit {
  loginObj: Login = new Login('', '');
  logged = true;
  logo = 'logo';
  schoolName: string;
  isLoading = false;
  isOtpSent = false;
  otp: number ;
  inputType = 'password';
  showPassword = false;
  @ViewChild('ack') ackModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  change = false;
  isPasswordMatched = false;
  newPassword: string ;
  passwordPattern = '^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!%*#?&])[A-Za-z0-9$@$!%*#?&]{8,}$';
  oldPassword: string;
  confirmPassword: string;
  constructor(private router: Router, private location: Location,
    public authenticationService: AuthenticationService,
    private tokenService: TokenService, private errorHandler: ErrorsHandler,
    private activateRoute: ActivatedRoute) {}

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.activateRoute.queryParams.subscribe((result => {
        this.change = result['change'];
    }));
    } else {
      this.authenticationService.logout();
    }
  }
  login(): void {
    if (this.loginObj.username.length > 0 && this.loginObj.password.length > 0) {
      this.isLoading = true;
      this.authenticationService.login(this.loginObj).subscribe((result) => {
        console.log(result);
        if (result.token) {
          this.isLoading = false;
          this.logged = true;
          if (result.schoolInfo) {
            this.logo = result.schoolInfo.schoollogo;
            this.schoolName = result.schoolInfo.schooldata[0].schoolname;
            if(result.menu !== '') {
              this.authenticationService.changeMenuState(result.menu.join());
              // this.commonService.changeMenu(result.menu.join());
              // localStorage.setItem("menuItems", result.menu.join());
            } else {
            console.log(result.menu)
            this.authenticationService.changeMenuState(result.menu);
          }
            this.authenticationService.changeLogoState(this.logo);
            localStorage.setItem('logo', this.logo);

          }
          this.router.navigate(['/home/dashboard']);
          this.tokenService.setUpOnLogin(result);
        } else {
          this.isLoading = false;
          this.logged = false;
          this.tokenService.setUpOnLogin(null);
          return
        }

      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    }
  }
  changePassword(): void {
    console.log(this.confirmPassword);
    if (this.newPassword === this.confirmPassword) {
      this.isLoading = true;
      this.authenticationService.changePassword(this.oldPassword,
        this.newPassword, this.tokenService.getSchoolId(), this.otp).subscribe((result: any) => {
        if (result.session) {
          this.isLoading = false;
          this.ackHead = 'Unauthorized';
          this.ackMsg = 'Please Login Again!';
          this.ackModal.show();
        } else if (result.status === 200) {
          this.isLoading = false;
          this.ackHead = 'Updated';
          this.ackMsg = 'Password is Updated';
          this.ackModal.show();
          this.authenticationService.logout();
        } else {
          this.isLoading = false;
          this.ackHead = 'Failed';
          this.ackMsg = result.result;
          this.ackModal.show();
        }
      } , error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
    } else {
      this.isPasswordMatched = true;
    }
  }
  homePage(): void {
    this.router.navigate(['/home/dashboard']);
  }
  getOtp() {
    setInterval(function() {
      this.isOtpSent = true;
    }, 120000);
    this.authenticationService.getOtp(this.oldPassword).subscribe((result: any) => {
      if (result.status === 200) {
        this.ackMsg = result.result;
        this.isOtpSent = true;
        console.log(result);
      } else if (result.status === 404) {
        this.ackHead = 'Not Found';
        this.ackMsg = 'Old Password is not matched';
        this.ackModal.show();
      }
    }, error => {
      this.isLoading = false;
      this.isOtpSent = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }

}
