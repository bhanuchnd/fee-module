import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Discount} from '../../models/discount/discount';
import {StudentTemplate} from '../../models/discount/studentTemplate';
import {Student} from '../../models/student';
import { Template } from '../../models/template/template';
import {CommonService} from '../../service/common.service';
import {DiscountService} from '../../service/discount.service';
import { TokenService } from '../../service/token.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';
@Component({
  templateUrl: 'discount.component.html',
  styleUrls: ['discount.component.css'],
  providers: [ErrorsHandler, DiscountService, CommonService, BsModalService, TokenService, AuthenticationService]
})
export class DiscountComponent implements OnInit {
  title = 'Add Discount';
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  search: string;
  discountDashboard: any;
  selected = false;
  index: number;
  isLoading = false;
  disabled = false;
  selectedTemplate: StudentTemplate = null;
  studentTemplates: StudentTemplate[];
  typeaheadNoResults = false;
  typeaheadLoading = false;
  dataSource: Student[] = [];
  discounts = {};
  created = false;
  student = null;
//  studentTemplate = new Template(-1, '', new Date(), Number(this.tokenService.getSchoolId()), -1);
  @ViewChild('modal') modal: ModalDirective;
   @ViewChild('ack') ackModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  isNew: boolean;
  discount: Discount = new Discount(-1, null, null, this.student);  // Set all proper values later
  constructor(private discountService: DiscountService,
    private modalService: BsModalService,
    private commonService: CommonService,
    private errorHandler: ErrorsHandler,
     private tokenService: TokenService,
     public authenticationService: AuthenticationService) {  }

  ngOnInit() {
    this.getDiscounts();
  }



  onCreated($event: ModalDirective) {
    // add dismiss reaction later to avoid esc or null
    if (this.created) {
      // this.getDiscounts();
    }
  }

  getDiscounts(): void {
    this.isLoading = true;

    this.discountService.getDiscounts().subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
    if (result.status === '200') {
      this.discountDashboard = result;
      this.discountDashboard.dashboard.totalDiscount = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(result.dashboard.totalDiscount);
      this.discountDashboard.dashboard.noOfstudentDiscountGiven = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(result.dashboard.noOfstudentDiscountGiven);
      }
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }

  public saveDiscount() {
    this.disabled = true;
    this.isLoading = true;
    this.discount.student = this.student;
    this.discount.id = this.selectedTemplate.id;
    this.selectedTemplate.discountFeeHeaderDefinitions.forEach(element => {
      element.discount = (this.discounts[element.feeHeader.id] / element.amount ) * 100;
    })
    console.log(this.selectedTemplate);
    this.discount.discountFeeHeaderDefinitions = this.selectedTemplate.discountFeeHeaderDefinitions;
    this.discount.name = this.selectedTemplate.name;
    this.discountService.createDiscount(this.discount).subscribe((result: any) => {
      this.disabled = false;
       this.isLoading = false;
       if (result.session) {
         this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
       }
        this.created = true;
        this.modal.hide();
        this.ackHead = 'Info';
        this.ackMsg = 'Discount has been offered for ' + this.discount.student.name;
        this.ackModal.show();
        this.getDiscounts();
      }, error => {
        this.isLoading = false;
        const err = this.errorHandler.handleError(error);
        if (err.isAuth) {
          this.ackHead = err.head;
          this.ackMsg = err.msg;
          // this.errorHandler.handleError(error);
          this.ackModal.show();
        }
      });
}
  public showModal(isNew, discount: Discount) {
    if (isNew) {
      this.modal.show();
    } else {
      this.title = 'Edit Discount';
      this.discount = discount;
      this.search = discount.student.name;
      this.discountService.getTemplates(discount.student.id).subscribe((result: any) => {
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
      if (result.status === '200') {
        this.studentTemplates = result.studentTemplates;
      } else {
        this.studentTemplates = [];
      }
    })
      this.modal.show();
    }

  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
  templateid(event): void {
    this.index = event.selectedIndex - 1;
    // console.log(this.studentTemplates);
    // this.selectedTemplate = this.studentTemplates[this.index];
    console.log(this.selectedTemplate);
    this.selectedTemplate.discountFeeHeaderDefinitions.forEach(element => {
      this.discounts[element.feeHeader.id] = Math.round((element.discount * element.amount) / 100);
    })
    console.log(this.discounts);
  }
  changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadLoading = true;
    this.commonService.getStudents({name: this.search}).subscribe((result) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (this.search.length > 0 && result && result.students.length <= 0) {
        this.typeaheadNoResults = true;
      }
      this.dataSource = result.students;
      this.typeaheadLoading = false;
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }
  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.selected = true;
    this.student = e.item;
    this.discountService.getTemplates(this.student.id).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (result.status === '200') {
        this.studentTemplates = result.studentTemplates;
      } else {
        this.studentTemplates = [];
      }
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ackModal.show();
      }
    });
  }


}
