import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';

import {TypeaheadModule} from 'ngx-bootstrap';
import {BsDatepickerModule} from 'ngx-bootstrap';


import { LoadingModule } from 'ngx-loading';
import {DiscountComponent} from './discount.component';
import {DiscountRoutingModule} from './discount-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DiscountRoutingModule,
    ChartsModule,
    FormsModule,
    LoadingModule,
    BsDropdownModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [DiscountComponent]
})
export class DiscountModule {}
