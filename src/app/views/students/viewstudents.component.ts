import { Class } from '../../models/class';
import { Section } from '../../models/section';
import { Student } from '../../models/student';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { CommonService } from '../../service/common.service';
import { StudentService } from '../../service/student.service';
import { TokenService } from '../../service/token.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  selector: 'app-viewstudents',
  templateUrl: 'viewstudents.component.html',
  styleUrls: ['./viewstudents.component.css'],
  providers: [TokenService, ErrorsHandler, CommonService, StudentService, AuthenticationService]
})
export class ViewstudentsComponent implements OnInit {
  classes: Class[];
  sections: Section[] = null;
  students: Student[] = null;
  classid: number
  isLoading = false;
  ackMsg: string;
  ackHead: string;
  sectionid: number;
  feeStudents: any;
  feeId: number;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  constructor(private studentService: StudentService,
    private route: ActivatedRoute,
    private commonService: CommonService, private tokenService: TokenService,
    private errorHandler: ErrorsHandler,
    public authenticationService: AuthenticationService) { }

  ngOnInit() {
   this.getClasses();
   this.route.params.subscribe( params => {
    if (params.fs_id) {
        this.feeId = params.fs_id;
        console.log(this.feeId);
        this.getFeeStudents();
    }

  });
  }


  getFeeStudents() {
    this.studentService.getFeeStudents(this.feeId).subscribe((result: any) => {
      if (result.status === 200) {
        this.feeStudents = result.fs_students;
      } else {
        this.feeStudents = [];
      }
    })
  }

   getClasses(): void {
     this.isLoading = true;
     this.commonService.getClasses().subscribe((result: any) => {
     this.isLoading = false;
     if (result.session) {
       this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
     }
     this.classes = result.classes;
     })
   }
  getSections(event): void {
    this.students = null;
    this.classid = event.value;
    this.sectionid = null;
    this.isLoading = true;
    this.commonService.getSections(this.classid).subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.sections = result.sections;
    })
  }
  getStudents(event): void {
    this.isLoading = true;
    this.sectionid = event.value;
    this.studentService.viewStudents(this.classid, this.sectionid).subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.students = result.students;
    })
  }
  getFeeStructureStudents() {

  }
}
