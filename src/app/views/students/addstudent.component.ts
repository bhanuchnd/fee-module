import { Class } from '../../models/class';
import { NewStudent } from '../../models/newstudent';
import { SpecialTemplate } from '../../models/specialTemplate';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { StudentService } from '../../service/student.service';
import { TokenService } from '../../service/token.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'ngx-dropdown-multiselect';
import 'rxjs/Rx' ;
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css'],
  providers: [TokenService, ErrorsHandler, CommonService, StudentService, AuthenticationService]
})
export class AddstudentComponent implements OnInit {
  newStudent: NewStudent = new NewStudent(null, null, [], null, null,
    null, null, null, null, null, null, null, new Date(1995, 0), 0, null);
  classes: Class;
  ackMsg: string;
  maxDate = new Date();
  ackHead: string;
  isLoading = false;
  disabled = false;
  mappedClasse = {};
  admSpecialTemplates: IMultiSelectOption[];
  selectedAdmTemplates: number[] = [];
  selectedTransTemplates: number[] = [];
  transSpecialTemplates: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-secondary col-md-12',
    dynamicTitleMaxItems: 1,
    showCheckAll: true,
    showUncheckAll: true,
    containerClasses: 'dropdown-menu',
    displayAllSelectedText: false
};
admTexts: IMultiSelectTexts = {
  checkAll: 'Select all',
  uncheckAll: 'Unselect all',
  checked: 'item selected',
  checkedPlural: 'items selected',
  searchPlaceholder: 'Find',
  searchEmptyResult: 'Nothing found...',
  searchNoRenderText: 'Type in search box to see results...',
  defaultTitle: 'Select Admission Templates',
  allSelected: 'All selected',
};
transTexts: IMultiSelectTexts = {
  checkAll: 'Select all',
  uncheckAll: 'Unselect all',
  checked: 'item selected',
  checkedPlural: 'items selected',
  searchPlaceholder: 'Find',
  searchEmptyResult: 'Nothing found...',
  searchNoRenderText: 'Type in search box to see results...',
  defaultTitle: 'Select Transpotation Templates',
  allSelected: 'All selected',
};

  @ViewChild('viewModal') viewModal: ModalDirective;
  @ViewChild('ack') ack: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  constructor(private router: Router, private studentService: StudentService,
    private modalService: BsModalService, private commonService: CommonService,
    private errorHandler: ErrorsHandler,
    private tokenService: TokenService, public authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.getClasses();
    this.getAdmTemplates();
    this.getTransTemplates();
  }

   getClasses(): void {
    this.commonService.getClasses().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
     this.classes = result.classes;
      result.classes.forEach(element => {
        this.mappedClasse[element.id] = element.name;
      })
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  getAdmTemplates() {
    this.studentService.getAdmTemplates().subscribe((result) => {
      this.admSpecialTemplates = result.fee_struc;
    })
  }
  getTransTemplates() {
    this.studentService.getTransTemplates().subscribe((result) => {
      this.transSpecialTemplates = result.fee_struc;
    })
  }
  onChange(event): void {
    // // console.log('WOrking');

    // this.newStudent.class.id = event;
  }
  addStudent(): void {
    this.viewModal.hide();
    this.isLoading = true;
    this.newStudent.id = -1;
    this.newStudent.schoolId = this.tokenService.getSchoolId();
    this.newStudent.class.name = this.mappedClasse[this.newStudent.class.id];
    this.newStudent.spl_fee = this.selectedAdmTemplates.concat(this.selectedTransTemplates).join();
    console.log(this.newStudent);
    this.disabled = true;
    this.studentService.createStudent(this.newStudent).subscribe((result: any) => {
      this.disabled = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      } else if (result.status === 200) {
        this.authenticationService.sync().subscribe((res) => {
          if (res.status === 200) {
            this.isLoading = false;
            if (result.student_id) {
              this.router.navigate(['/home/transaction', {id: result.student_id}]);
            } else {
              this.ackHead = 'Info';
              this.ackMsg = 'Student is Created. No Fee Structure to make a transaction';
              this.ack.show();
            }
          } else {
            this.isLoading = false;
            this.ackHead = 'Info';
            this.ackMsg = 'Something went wrong. Please Go to Transaction Page and search for the New Student';
            this.ack.show();
          }
        })
      } else {
        this.isLoading = false;
        this.ackHead = 'Info';
        this.ackMsg = 'Failed while creating the student';
        this.ack.show();
      }

    }, error => {
      this.viewModal.hide();
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
}
