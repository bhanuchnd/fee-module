import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsRoutingModule } from './students-routing.module';
import { AddstudentComponent } from './addstudent.component';
import { FormsModule } from '@angular/forms';
import { SectionmapComponent } from './sectionmap.component';
import { ViewstudentsComponent } from './viewstudents.component';
import { ModalModule } from 'ngx-bootstrap';
import { MultiselectDropdownModule } from 'ngx-dropdown-multiselect';
import { LoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    StudentsRoutingModule,
    FormsModule,
    LoadingModule,
    MultiselectDropdownModule,
    ModalModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [AddstudentComponent, SectionmapComponent, ViewstudentsComponent]
})
export class StudentsModule { }
