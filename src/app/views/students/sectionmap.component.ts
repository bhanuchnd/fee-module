import { Class } from '../../models/class';
import { Section } from '../../models/section';
import { NewStudent } from '../../models/newstudent';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { StudentService } from '../../service/student.service';
import { TokenService } from '../../service/token.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings } from 'ngx-dropdown-multiselect';
import { AuthenticationService } from '../../service/authentication.service';
import { AppComponent } from '../../app.component';
import { ErrorsHandler } from '../../errors-handler';


@Component({
  selector: 'app-sectionmap',
  templateUrl: './sectionmap.component.html',
  styleUrls: ['./sectionmap.component.css'],
  providers: [TokenService, ErrorsHandler, CommonService, StudentService, AuthenticationService]
})
export class SectionmapComponent implements OnInit {
  @ViewChild('ack') ack: ModalDirective;
  @ViewChild('confirm') confirm: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  ackMsg: string;
  ackHead: string;
  isLoading = false;
  studentId: number;
  students: NewStudent[];
  sections: Section[];
  studentSection: any = {};
  classes: Class;
  disabled = false;
  options: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-secondary col-md-8',
    dynamicTitleMaxItems: 2,
    showCheckAll: true,
    showUncheckAll: true,
    containerClasses: 'dropdown-menu',
    displayAllSelectedText: true
};
  selectedClasses: number[];
  constructor(private studentService: StudentService,
     private commonService: CommonService,
     private errorHandler: ErrorsHandler,
     public authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.getClasses();
  }
  getClasses(): void {
    this.commonService.getClasses().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
    this.classes = result.classes;
    this.options = result.classes;
    // console.log(this.options);
    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  onchange(index) {
    // console.log(this.students[index].fee_structure_ids);
  }
  setSelected(target): void {
    this.selectedClasses = target;
    if (this.selectedClasses.length === this.options.length) {
      document.getElementsByClassName('dropdown-menu')[0]['style']['display'] = 'none';
    }
  }
  setStudent(student) {
    this.studentId = student.id;
    this.ackHead = 'Delete Student?';
    this.ackMsg = 'Are you sure want to delete';
    this.confirm.show();
  }
  getStudents(): void {
      if (this.selectedClasses.length > 0) {
        this.isLoading = true;
        this.studentService.getStudents(this.selectedClasses).subscribe((result: any) => {
          if (result.session) {
            this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
          }
          this.isLoading = false;
          this.students = result.students;
          // console.log(result.students);
          // console.log(this.students);
        }, error => {
          this.isLoading = false;
            this.ackHead = 'Failed to Connect';
            this.ackMsg = 'Please check your internet connection and try again!';
            if (!this.ack.isShown) {
              this.ack.show();
            }
        })
      } else {
          this.ackHead = 'Info';
          this.ackMsg = 'Please select the Classes';
          this.ack.show();
      }
  }
  mapsection(student: NewStudent): void {
    if (student.section.id !== 0) {
      this.isLoading = true;
      if (!student.fee_structure_ids) {
        student.fee_structure_ids = [];
      }
      // if(student.class_feestructures.length > 0 && student.fee_structure_ids.length > 0) {
      this.disabled = true;
        this.studentService.mapSections(student).subscribe((result: any) => {
          this.disabled = false;
        if (result.session) {
          this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
        }
        this.isLoading = false;
          if (result.success === true) {
            this.authenticationService.sync().subscribe((res) => {
              if (res.status === 200) {
                this.ackHead = 'Info';
                this.ackMsg = student.name + ' mapped to section ' + student.section.name;
                this.ack.show();
              }
            })
          } else {
              this.ackHead = 'Info';
              this.ackMsg = 'Failed while mapping. Please try again!'
              this.ack.show();

          }
        }, error => {
          this.disabled = false;
          this.isLoading = false;
          const err = this.errorHandler.handleError(error);
          if (err.isAuth) {
            this.ackHead = err.head;
            this.ackMsg = err.msg;
            // this.errorHandler.handleError(error);
            this.ack.show();
          }
        });
      } else {
        this.ackHead = 'Info';
        this.ackMsg = 'Please select the section to map';
        this.ack.show();
    }
  }

  deleteStudent(studentid): void {
    this.studentService.deleteStudent(studentid).subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
    if (result.success === true) {
      this.authenticationService.sync().subscribe((res) => {
        if (res.status === 200) {
        this.ackHead = 'Info';
        this.ackMsg = 'Student has been deleted';
        this.ack.show();
        }
      });
    }
    }, error => {
        this.ackHead = 'Failed to Connect';
        this.ackMsg = 'Please check your internet connection and try again!';
        this.ack.show();
    })
    this.confirm.hide();
  }
}
