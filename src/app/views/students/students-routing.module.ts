import { AddstudentComponent } from './addstudent.component';
import {SectionmapComponent} from './sectionmap.component';
import {ViewstudentsComponent} from './viewstudents.component';
import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    data: {
      title: ''
    },
     children: [
      {
        path: 'newadmission',
        component: AddstudentComponent,
        data: {
          title: 'New Admission'
        }
      },
      {
        path: 'map-section',
        component: SectionmapComponent,
        data: {
          title: 'Map Sections for Student'
        }
      },
      {
        path: 'view-students',
        component: ViewstudentsComponent,
        data: {
          title: 'View Students'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
