import { CommonService } from '../../service/common.service';
import { TokenService } from '../../service/token.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../service/authentication.service';
import {Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { ErrorsHandler } from '../../errors-handler';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ErrorsHandler, CommonService, TokenService, AuthenticationService, BsModalService]
})

export class DashboardComponent implements OnInit {
  modalRef: BsModalRef;
  public brandPrimary = '#20a8d8';
  public brandSuccess = '#4dbd74';
  public brandInfo = '#63c2de';
  public brandWarning = '#f8cb00';
  public brandDanger = '#f86c6b';
  @ViewChild('ack') ackModal: ModalDirective;
  @ViewChild('modal') modal: TemplateRef<any>;
  ackMsg = 'Please Login Again';
  ackHead = 'Session Expired';
  isLoading = false;
  totalStudents;
  totalDue;
  totalPaid;
  cf: any;
  overDue;

  // dropdown buttons
  // public status: { isopen } = { isopen: false };
  // public toggleDropdown($event: MouseEvent): void {
  //   $event.preventDefault();
  //   $event.stopPropagation();
  //   this.status.isopen = !this.status.isopen;
  // }

  // lineChart
    public lineChartData: Array<any>
   = null;
  public lineChartLabels: Array<any> = null;
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
    legend: {
      display: false
    }
  };
    public lineChartColours: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public barChartColors = [{ // grey
      backgroundColor: '#2980b9',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  { // grey
      backgroundColor: '#e74c3c',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#f39c12',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#16a085',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#63c2de',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#2c3e50',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#f1c40f',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
      backgroundColor: '#2980b9',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  { // grey
      backgroundColor: '#e74c3c',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#f39c12',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#16a085',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#63c2de',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#2c3e50',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#f1c40f',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
      backgroundColor: '#2980b9',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  { // grey
      backgroundColor: '#e74c3c',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#f39c12',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#16a085',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
{ // grey
      backgroundColor: '#63c2de',
      borderColor: '#20a8d8',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#2c3e50',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // grey
          backgroundColor: '#f1c40f',
          borderColor: '#20a8d8',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  // barChart
  public pieChartLabels: string[] = null;
  public pieChartData: number[] = null;
  public barChartLabels = null;
  public pieChartType = 'pie';
  public barChartData = null
  public barChartOptions: any = {
     tooltips: {
      callbacks: {
         title: function() {}
      }
   },
    scaleShowVerticalLines: false,

    legend: {
      display: false
    },
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true

                }
            }],
            xAxes: [{
              categoryPercentage: 1
            }
//              ,{
//              gridLines: {
//                offsetGridLines: true
//            }
//              }
            ]
        }
  };
  public barChartType = 'bar';
  public barChartLegend = true;

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private tokenService: TokenService, private commonService: CommonService,
    private location: Location, public authenticationService: AuthenticationService,
    private errorHandler: ErrorsHandler,
     private modalService: BsModalService) {}

  ngOnInit(): void {
    this.location.subscribe(x => {
      if (x.url === '') {
        this.tokenService.setUpOnLogin(null);
    }
    });
    this.dashboard();

  }

  dashboard(): void {
    this.cf = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      });
      if (this.tokenService.getToken()) {
        this.isLoading = true;
        this.commonService.dashboard().subscribe((result: any) => {
          this.isLoading = false;
          if (result.status === '200') {
            console.log(result);
            this.totalStudents = new Intl.NumberFormat('en-IN', {}).format(result.dashboard.totalStudents);
            this.totalPaid = new Intl.NumberFormat('en-IN', {}).format(result.dashboard.totalPaid);
            this.totalDue = this.cf.format(result.dashboard.totalDue);
            this.overDue = this.cf.format(result.dashboard.overDue);
            this.lineChartData = result.dashboard.lineChartY;
            this.lineChartLabels = result.dashboard.lineChartX;
            this.barChartData = result.dashboard.pieChartY;
            this.barChartLabels = result.dashboard.pieChartX;
          } else if (result.session) {
            this.authenticationService.sessionExpired(this.ackModal, this.ackHead, this.ackMsg);
          } else {
            this.ackMsg = 'Failed to connect Server';
            this.ackHead = 'Connection Error';
            this.ackModal.show();
          }

        }, error => {
          this.isLoading = false;
          const err = this.errorHandler.handleError(error);
          if (err.isAuth) {
            this.ackHead = err.head;
            this.ackMsg = err.msg;
            // this.errorHandler.handleError(error);
            this.ackModal.show();
          }
        });
      }
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    if (!this.tokenService.getToken()) {
      this.ackMsg = 'Please Login Again!';
      this.ackHead = 'Session Expired';
      this.ackModal.show();
      // this.modalRef = this.modalService.show(this.modal, {keyboard: false})
    }
  }
}
