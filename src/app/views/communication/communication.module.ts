import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import { LoadingModule } from 'ngx-loading';
import { CommunicationRoutingModule } from './communication-routing.module';
import { CommunicationComponent } from './communication.component';

@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule,
    LoadingModule,
    CommunicationRoutingModule,
    FormsModule,
    ModalModule.forRoot()
  ],
   schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [CommunicationComponent]
})
export class CommunicationModule { }
