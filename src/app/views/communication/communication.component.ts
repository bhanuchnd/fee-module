import { SmsTemplates } from '../../models/comm/smstemplates';
import { StakeHolder } from '../../models/comm/stakeHolder';
import { ExternalCommService } from '../../service/external-comm.service';
import {Component, OnInit, ViewChild, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { AppComponent } from '../../app.component';
import { InternalCommService } from '../../service/internal-comm.service';
import { TokenService } from '../../service/token.service';
import { AuthenticationService } from '../../service/authentication.service';
import { ErrorsHandler } from '../../errors-handler';


@Component({
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css'],
  providers: [BsModalService, InternalCommService, ErrorsHandler, TokenService, ExternalCommService, AuthenticationService]
})
export class CommunicationComponent implements OnInit {
  stakeHolders: any;
  authorities: any;
  isLoading = false;
  ackMsg: string;
  disabled = false;
  ackHead: string;
  smsTemplates: SmsTemplates[];
  @ViewChild('authorityModal') modal: ModalDirective;
  @ViewChild('sessionModal') sessionModal: ModalDirective;
  @ViewChild('ack') ack: ModalDirective;
  stakeHolder: StakeHolder = new StakeHolder(-1, this.tokenService.getSchoolId(), -1, '', '');
  constructor(private internalCommService: InternalCommService,
     private externalCommService: ExternalCommService,
     private errorHandler: ErrorsHandler,
      private tokenService: TokenService, public authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.getStakeHolders();
    this.getAuthorities();
    this.getSmsTemplates();
  }

  getStakeHolders(): void {
    this.internalCommService.getstakeHolders().subscribe((result: any) => {
      if (result.status === 200) {
        this.stakeHolders = result.stakeholders;
        // console.log(this.stakeHolders);
      } else if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      } else {
        this.ackMsg = 'Failed to connect Server';
        this.ackHead = 'Connection Error';
        this.ack.show();
      }

    }, error => {
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }


  getAuthorities(): void {
    this.internalCommService.getAuthorities().subscribe((result: any) => {
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.authorities = result.stakeholders;
      // console.log(this.authorities);
    })
  }
  onChange(event): void {
    this.stakeHolder = this.stakeHolders[event.selectedIndex - 1];
    // console.log(this.stakeHolder);
  }
  saveAuthority(): void {
    this.disabled = true;
    this.isLoading = true;
    this.internalCommService.createAuthority(this.stakeHolder).subscribe((result: any) => {
      this.isLoading = false;
      this.isLoading = false;
      this.modal.hide();
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      if (result.status === 200) {
        this.ackHead = 'Updated';
        this.ackMsg =  this.stakeHolder.name + ' phone number has been updated successfully';
        this.getAuthorities();
      } else if (result.status === 401) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      } else {
        this.ackHead = 'Error';
        this.ackMsg = this.stakeHolder.name + ' phone number is not getting updated. Please try again';
      }
      this.ack.show();
    }, error => {
      this.modal.hide();
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  getSmsTemplates(): void {
    this.isLoading = true;
    this.externalCommService.getSmsTemplates().subscribe((result: any) => {
      this.isLoading = false;
      this.smsTemplates = result.templates;
    }, error => {
      this.modal.hide();
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
  saveTemplate(template): void {
    this.isLoading = true;
    // console.log(template);
    this.externalCommService.updateTemplate(template).subscribe((result: any) => {
      this.isLoading = false;
      if (result.session) {
        this.authenticationService.sessionExpired(this.sessionModal, this.ackHead, this.ackMsg);
      }
      this.getSmsTemplates();
      if (result.status === 200) {
        this.ackHead = 'Info';
        this.ackMsg = 'SMS Template has been updated';
      } else {
        this.ackHead = 'Info';
        this.ackMsg = 'SMS Template has failed. Please try again';
      }
      this.ack.show();
    }, error => {
      this.modal.hide();
      this.isLoading = false;
      const err = this.errorHandler.handleError(error);
      if (err.isAuth) {
        this.ackHead = err.head;
        this.ackMsg = err.msg;
        // this.errorHandler.handleError(error);
        this.ack.show();
      }
    });
  }
}
