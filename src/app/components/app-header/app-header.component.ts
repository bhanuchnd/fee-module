import {AuthenticationService} from '../../service/authentication.service';
import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  providers: [AuthenticationService]
})
export class AppHeaderComponent implements OnChanges {
  @Input() schoolLogo = localStorage.getItem('logo');
  spin = 'fas fa-sync-alt fa-spin';
  withoutSpin = 'fas fa-sync-alt';
  isSyncing = false;
  @Output() logoChanged: EventEmitter<string>= new EventEmitter<string>();
  constructor(private authenticationService: AuthenticationService) {
    // this.sync();
    // setInterval(() => {
    //   this.sync();
    // }, environment.interval);
  }
  logout() {
    this.authenticationService.logout();
  }
  sync() {
    this.isSyncing = true;
    this.authenticationService.sync().subscribe((result) => {
      this.isSyncing = false;
    }, error => {
      this.isSyncing = false;
    });
  }
  ngOnChanges() {
    this.logoChanged.emit(this.schoolLogo);
    console.log(this.schoolLogo);
  }

}
