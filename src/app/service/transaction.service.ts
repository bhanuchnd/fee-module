import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClientService} from './api.service';
import {Student} from '../models/student';
import {Transaction} from '../models/transaction/transaction';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TransactionService {
  constructor(private httpService: HttpClientService) {
  }


  getTransactions(): Observable<any> {
    return
  }

  /*getStudents(token: string): Observable<Student[]> {
    return
  } */

  getTransactonHistory(studentId): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/transaction', {studentId: studentId})
  }
  getDashboard(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/transactionDashboard', null);
  }

  createTransaction(transaction: Transaction): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/transaction', transaction)
  }
  getModes(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/modeofpayment', null)
  }
  printReceipt(transactionId, templateId, studentId, invoiceType): Observable<any> {
    return this.httpService.getPdf(environment.API_ENDPOINT + '/invoiceCollection',
     {invoiceType: invoiceType, studentId: studentId, transactionId: transactionId, templateId: templateId })
  }
}
