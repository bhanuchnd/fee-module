import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClientService} from './api.service';
import {Classification} from '../models/template/classification';
import {FeeHeader} from '../models/template/feeHeader';
import {FeeStructure} from '../models/template/feeStructure';
import {Template} from '../models/template/template';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TemplateService {

  constructor(private httpService: HttpClientService) {

  }

  /* Used to get Classificatin Names CRUD*/
  getTemplateClassifications(): Observable<Classification[]> {
    return this.httpService.get<Classification>(environment.API_ENDPOINT + '/template_classification', null);
  }

  createTemplateClassification(classification: Classification): Observable<any> {
    return this.httpService.post<Classification>(environment.API_ENDPOINT + '/template_classification', classification);

  }

  deleteTemplateClassification(id: number): Observable<{}> {
    return this.httpService.delete<Classification>(environment.API_ENDPOINT + '/template_classification', {id: id});
  }

  updateTemplateClassification(classification: Classification): Observable<any> {
    return this.httpService.update<Classification>(environment.API_ENDPOINT + '/template_classification', classification);
  }



  /* Used to get Template Names CRUD*/

  getTemplates(): Observable<Template[]> {
    return this.httpService.get<Template>(environment.API_ENDPOINT + '/template', null);
  }

  createTemplate(template: Template): Observable<any> {
    return this.httpService.post<Template>(environment.API_ENDPOINT + '/template', template);

  }

  deleteTemplate(id: number): Observable<{}> {
    return this.httpService.delete<Template>(environment.API_ENDPOINT + '/template', {id: id});
  }

  updateTemplate(template: Template): Observable<any> {
    return this.httpService.update<Template>(environment.API_ENDPOINT + '/template', template);
  }

  /* Used for Fee Headers */

  getTemplateFeeHeaders(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/fee_header', null);
  }
  createTemplateFeeHeader(header: FeeHeader): Observable<any> {
    return this.httpService.post<FeeHeader>(environment.API_ENDPOINT + '/fee_header', header);

  }

  deleteTemplateFeeHeader(id: number): Observable<{}> {
    return this.httpService.delete<FeeHeader>(environment.API_ENDPOINT + `/fee_header`, {id: id});
  }

  updateTemplateFeeHeader(header: FeeHeader): Observable<any> {
    return this.httpService.update<FeeHeader>(environment.API_ENDPOINT + '/fee_header', header);
  }


  /* Used for Fee Structure */

  getFeeStructures(): Observable<FeeStructure[]> {
    return this.httpService.get<FeeStructure>(environment.API_ENDPOINT + '/feestructure', null);
  }
  createFeeStructure(feeStructure: FeeStructure): Observable<FeeStructure> {
    return this.httpService.post<FeeStructure>(environment.API_ENDPOINT + '/feestructure', feeStructure);

  }

  deleteFeeStructure(id: number): Observable<{}> {
    return this.httpService.delete<FeeStructure>(environment.API_ENDPOINT + '/feestructure', {id: id});
  }

  updateFeeStructure(feeStructure: FeeStructure): Observable<any> {
    return this.httpService.update<FeeStructure>(environment.API_ENDPOINT + '/feestructure', feeStructure);
  }


}