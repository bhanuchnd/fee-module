import {environment} from '../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';

import {httpOptions} from '../common/headers';
import {HttpClientService} from './api.service';
import { StakeHolder } from '../models/comm/stakeHolder';

@Injectable()
export class InternalCommService {

  constructor(private httpService: HttpClientService) {}
  getstakeHolders(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/fetchStakeholders', null);
  }
  getAuthorities(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/internalCommunication', null);
  }
  createAuthority(stakeHolder: StakeHolder): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/internalCommunication',
     {schoolId: stakeHolder.schoolId, stakeHolderId: stakeHolder.stakeHolderId, mobile: stakeHolder.mobile});
  }

}
