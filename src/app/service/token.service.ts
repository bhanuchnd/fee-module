import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class TokenService {
  constructor(public jwtHelper: JwtHelperService) {}
  public getToken(): string {
    if (localStorage.getItem('token')) {
      return localStorage.getItem('token');
    } else {
      return null;
    }
  }
  public getSchoolId(): string {
    if (localStorage.getItem('schoolId')) {
      return localStorage.getItem('schoolId');
    } else {
      return null;
    }

  }


  public setUpOnLogin(result: any): void {
    if (result && result.token && result.token.length > 0) {
      localStorage.setItem('token', result.token);
      localStorage.setItem('schoolId', result.schoolId);
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('schoolId');
    }
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return this.jwtHelper.isTokenExpired();
  }
}
