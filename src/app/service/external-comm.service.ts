import {environment} from '../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';

import {httpOptions} from '../common/headers';
import {HttpClientService} from './api.service';

@Injectable()
export class ExternalCommService {

  constructor(private httpService: HttpClientService) {}
  getSmsTemplates(): Observable<{}> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/parentCommunication', null);
  }
  updateTemplate(smsTemplates): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/parentCommunication', smsTemplates);
  }

}
