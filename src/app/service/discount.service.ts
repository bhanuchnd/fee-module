/**
 * New typescript file
 */
import { environment } from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClientService} from './api.service';
import {Discount} from '../models/discount/discount';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DiscountService {

  constructor(private httpService: HttpClientService) {

  }

  getDiscounts(): Observable<{}> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/discountDashboard', null);
  }
  createDiscount(discount: Discount): Observable<Discount> {
    return this.httpService.post<Discount>(environment.API_ENDPOINT + '/discount', discount);
  }

  deleteDiscount(id: number): Observable<{}> {
    return this.httpService.delete<Discount>(environment.API_ENDPOINT + '/discount', {id: id});
  }

  updateDiscount(discount: Discount): Observable<Discount> {
    return this.httpService.update<Discount>(environment.API_ENDPOINT + '/discount', discount);
  }
  getTemplates(studentId): Observable<{}> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/getStudentTemplates', {studentId: studentId})
  }
}

