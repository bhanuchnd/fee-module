/**
 * Authentication Service
 */
/* Used for Login and Logout and other api calls
 */


/**
 * New typescript file
 */
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

import {TokenService} from './token.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';

import {httpOptions} from '../common/headers';
import {HttpClientService} from './api.service';
import {environment} from '../../environments/environment';
import {Login} from '../models/login';
import {Router} from '@angular/router';
@Injectable()
export class AuthenticationService {
  ackMsg = '';
  ackHead = '';
  change = 1;
  menuState = new BehaviorSubject("");
  logoState = new BehaviorSubject("logo");
  menuItems = this.menuState.asObservable();
  schoolLogo = this.logoState.asObservable();
  constructor(private httpService: HttpClientService, private tokenService: TokenService, private router: Router) {

  }
  changeMenuState(menuItems: string) {
    this.menuState.next(menuItems);
  }
  changeLogoState(logo: string) {
    this.logoState.next(logo);
  }

  sync(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/data', null);
  }
  login(loginParams): Observable<any> {
    return this.httpService.loginPost<Login>(environment.API_ENDPOINT + '/login', loginParams);

  }

  getSchoolsInfo(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/schools_info', null);
  }
  sessionExpired(modal, header: string, msg: string) {
    this.ackHead = 'Session Expired';
    this.ackMsg = 'Please login Again';
    header = 'Session Expired';
    msg =  'Please login Again';
    modal.show();
  }
  getOtp(oldPassword): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/getOTP', {data: oldPassword});
  }

  logout(): boolean {
    this.menuState.next('');
    this.tokenService.setUpOnLogin(null);
    this.router.navigate(['/']);
    return false;
  }
  changePassword(oldPassword: string, newPassword: string, schoolId, otp): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/changepassword',
     {old_password: oldPassword, new_password: newPassword, schoolId: schoolId, otp: otp })
  }

}
