import {environment} from '../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';

import {httpOptions} from '../common/headers';
import { NewStudent } from '../models/newstudent';
import {HttpClientService} from './api.service';


@Injectable()
export class StudentService {

  constructor(private httpService: HttpClientService) {}
  createStudent(newStudent: NewStudent): Observable<any> {
    return this.httpService.post<NewStudent>(environment.API_ENDPOINT + '/newadmission', newStudent);
  }
  getStudents(selectedClass: number[]): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/mapsection', {classIds: selectedClass});
  }
  mapSections(students: NewStudent): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/mapsection', students);
  }
  deleteStudent(studentid: any): Observable<any> {
    return this.httpService.delete<any>(environment.API_ENDPOINT + '/newadmission', {id: studentid});
  }
  viewStudents(classid, sectionid): Observable<any> {
     return this.httpService.get<any>(environment.API_ENDPOINT + '/students', {classId: classid, sectionId: sectionid});
  }
  getFeeStudents(feeId): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/getFeeStrucStudents', {fs_id: feeId});
  }
  getAdmTemplates(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/getadmissionDefault', {type: 'adm'})
  }
  getTransTemplates(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/getadmissionDefault', {type: 'trans'})
  }
}
