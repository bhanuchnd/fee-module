/**
 * New typescript file
 */
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';


import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';

import {httpOptions} from '../common/headers';
import {Student} from '../models/student';
import {Class} from '../models/class';
import { NewStudent } from '../models/newstudent';
import { Section } from '../models/section';
import {HttpClientService} from './api.service';

@Injectable()
export class CommonService {
  // menuState = new BehaviorSubject("");
  // menuItems = this.menuState.asObservable();
  constructor(private httpService: HttpClientService) {
  }
  // changeMenu(menuItems: string) {
  //   this.menuItems.subscribe(menuItem => console.log(menuItem.length));
  //   this.menuState.next(menuItems);
  //   this.menuItems.subscribe(menuItem => console.log(menuItem));
  // }
  getStudents(params): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/schoolstudents', params);
  }
  getClasses(): Observable<Class[]> {
    return this.httpService.get<Class>(environment.API_ENDPOINT + '/classes', null)
  }
  getMenuItems(): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/menu', null)
  }
  mapMenuItems(menuItems, id, schoolId): Observable<any> {
    return this.httpService.post<any>(environment.API_ENDPOINT + '/menu', {menu_ids: menuItems, id: id, schoolId: schoolId});
  }
   getStudentClasses(): Observable<Class[]> {
    return this.httpService.get<Class>(environment.API_ENDPOINT + '/classeshasstudents', null)
  }
  downloadReport(reportType, fromDate, toDate, templateId, classifyId, classId) {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/report',
    {reportType: reportType, from: fromDate, to: toDate, templateId: templateId, tc_id: classifyId, class: classId});
  }
  downloadNewReport(reportType, fromDate, toDate, templateId, classifyId, classId, isGrouped) {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/report_new',
    {reportType: reportType, from: fromDate, to: toDate, templateId: templateId, tc_id: classifyId, class: classId, group_flag: isGrouped});
  }
  dashboard(): Observable<Class[]> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/dashboard', null)
  }
  data(schoolId): Observable<any> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/data', {schoolId: schoolId});
  }
  getSections(classid): Observable<Section[]> {
    return this.httpService.get<any>(environment.API_ENDPOINT + '/sections', {classId: classid});
  }
}
