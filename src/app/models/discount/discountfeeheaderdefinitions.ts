import { FeeHeader } from '../template/feeHeader';
export class DiscountFeeHeaderDefinitions {
  id: number;
  feeHeader: FeeHeader;
  amount: number;
  discount: number;
  constructor(id,feeHeader: FeeHeader,amount,discount) {
    this.id = id;
    this.feeHeader = feeHeader;
    this.amount = amount;
    this.discount = discount;
  }
}