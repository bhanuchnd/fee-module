import { DiscountFeeHeaderDefinitions } from './discountfeeheaderdefinitions';
export class StudentTemplate {
  id: number;
  name: string;
  isFreeze: number;
  discountFeeHeaderDefinitions: DiscountFeeHeaderDefinitions[];
  constructor(id, name, discountFeeHeaderDefinitions: DiscountFeeHeaderDefinitions[], isFreeze ) {
    this.id = id;
    this.name = name;
    this.discountFeeHeaderDefinitions = discountFeeHeaderDefinitions;
    this.isFreeze = isFreeze;
  }
}