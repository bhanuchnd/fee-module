import {Student} from '../student';
import { DiscountFeeHeaderDefinitions } from './discountfeeheaderdefinitions';

export class Discount {
  student: Student;
   id: number;
  name: string;
  discountFeeHeaderDefinitions: DiscountFeeHeaderDefinitions[];
  constructor(id, name, discountFeeHeaderDefinitions: DiscountFeeHeaderDefinitions[], student: Student ) {
    this.id = id;
    this.name = name;
    this.discountFeeHeaderDefinitions = discountFeeHeaderDefinitions;
    this.student = student;
  }
}