import {HeaderDefinition} from './headerDefinition';
import {Class} from '../class';
import {Template} from './template';
import {MappedStudent} from '../mappedStudent';
export class FeeStructure {
  id: number;
  template: Template;
  is_class_template: number;
  students: MappedStudent[];
  student_ids: number[];
  feeHeaderDefinitions: Array<HeaderDefinition>;
  class: Class;
  isFreeze: number;
  total: number;

  constructor(id: number, template: Template, student_ids, students, feeHeaderDefinitions: Array<HeaderDefinition>, aclass: Class, isFreeze,total,is_class_template) {
    this.id = id;
    this.template = template;
    this.class = aclass;
    this.feeHeaderDefinitions = feeHeaderDefinitions;
    this.is_class_template = is_class_template;
    this.isFreeze = isFreeze;
    this.students = students;
    this.total = total;
  }

}
