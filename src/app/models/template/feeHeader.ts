
/* Independently create the fee headers . can be assosciated to template */
export class FeeHeader {
  id: number;
  name: string;
  schoolId: number;
  isFreeze: number;

  constructor(id: number, name: string, schoolId: number, isFreeze: number) {
    this.id = id;
    this.name = name;
    this.schoolId = schoolId;
    this.isFreeze = isFreeze;
  }


}


