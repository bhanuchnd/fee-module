
export class MapFeeStructStudent {
  id: number;
  feeStructId: number;
  studentId: number;
  classId: number;
  sectionId: number;
  isClassSelected: boolean;
  isSectionSelected: boolean;

}

