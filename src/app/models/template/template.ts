/* one to many relation with classification . Many templates can be assigend to one classification ID */

export class Template {
  id: number;
  tempClassRefId: number;
  name: string;
  overdueDate: Date;
  schoolId: number;
  isFreeze: number;

  constructor(id: number, name: string, overdueDate, schoolId: number, tempClassRefId, isFreeze: number) {

    this.id = id;
    this.name = name;
    this.overdueDate = overdueDate;
    this.schoolId = schoolId;
    this.isFreeze = isFreeze;
    this.tempClassRefId = this.tempClassRefId;

  }


}

