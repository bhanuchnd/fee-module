export class Classification {
  id: number;
  name: string;
  schoolId: number;
  isFreeze: number;

  constructor(id: number, name: string, schoolId: number, isFreeze: number) {
    this.id = id;
    this.name = name;
    this.schoolId = schoolId;
    this.isFreeze = isFreeze;
  }

  public getId(): number {
    return this.id;
  }

  public setId(id): void {
    this.id = id;
  }
}

