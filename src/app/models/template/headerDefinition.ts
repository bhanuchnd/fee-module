/* Define the fee headers in creatng fee structure and assigning to template. Can be array of fee headers to one 

temp id and one fee struct id */

import {FeeHeader} from './feeHeader';
export class HeaderDefinition {
  feeHeader: FeeHeader;
  amount: number;

  constructor(feeHeader: FeeHeader, amount: number) {
    this.feeHeader = feeHeader;
    this.amount = amount;
  }

}


