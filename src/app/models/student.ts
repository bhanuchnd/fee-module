/**
 * Student Class
 */
export class Student {
  id: number;
  name: string;
  class: string;
  section: string;
  father: string;
  phone: number;
  address: string;
  constructor(id, name, aclass, section, father, phone, address) {
    this.id = id;
    this.name = name;
    this.class = aclass;
    this.section = section;
    this.father = father;
    this.phone = phone;
    this.address = address;
  }
}
