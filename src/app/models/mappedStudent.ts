export class MappedStudent {
  id: number;
  name: string;
  isFreeze: number;
  className: string;
  section: string;
  constructor(id ,name, isFreeze, className, section) {
    this.id = id;
    this.name = name;
    this.isFreeze = isFreeze;
    this.className = className;
    this.section = section;
  }
}
