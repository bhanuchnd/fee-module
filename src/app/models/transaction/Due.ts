/**
 * New typescript file
 */
import {HeaderDefinition} from '../template/headerDefinition';
import {Template} from '../template/template';
export class Due {
  id: number;
  template: Template;
  feeHeaderDefinitions: Array<HeaderDefinition>;
  due: number;
  paid: number;
  total: number;
  isOverDue: number;
}
