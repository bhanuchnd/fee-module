/**
 * New typescript file
 */
import {Student} from '../student';
import {HeaderDefinition} from '../template/headerDefinition';
import {Template} from '../template/template';
import {Due} from './Due';
import {Mode} from './mode';
export class Transaction {
  id: number;
  template: Template;
  student: Student;
  transaction_datetime: any;
  feeHeaderDefinitions: Array<HeaderDefinition>;
  due: number;
  paid: number;
  is_receipt: number;
  mode: Mode;
  creditCardName: string;
  creditCardNo: number;
  debitCardName: string;
  debitCardNo: number;
  bankName: string;
  chequeNo: number;
  chequeDate: string;
}
