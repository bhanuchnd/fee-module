/**
 * New typescript file
 */
import {Student} from '../student';
import {Due} from './Due';
import {Transaction} from './transaction';
export class TransactionHistory {

  student: Student;
  totalPaidAmount: number;
  totalDueAmount: number;
  CurrentDues: Array<Due>;
  transactions: Array<Transaction>;
}
