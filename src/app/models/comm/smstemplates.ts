export class SmsTemplates {
  id: number;
  schoolId: number;
  defaultCommTemplateId: number;
  name: string;
  template: string;
  enableAlert: number;
  constructor(id, schoolId, defaultCommTemplateId, name, template, enableAlert ) {
    this.id = id;
    this.schoolId = schoolId;
    this.defaultCommTemplateId = defaultCommTemplateId;
    this.name = name;
    this.template = template;
    this.enableAlert = enableAlert;
  }
}