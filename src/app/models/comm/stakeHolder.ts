export class StakeHolder {
  id: number;
  schoolId: number;
  stakeHolderId: number;
  name: string;
  mobile: string;
  constructor(id, schooolId, stakeHolderId, name, mobile) {
    this.id = id;
    this.schoolId = schooolId;
    this.stakeHolderId = stakeHolderId;
    this.name = name;
    this.mobile = mobile;
  }
}