import { Class } from './class';
import { Section } from './section';
import { IMultiSelectOption } from 'ngx-dropdown-multiselect';
export class NewStudent {
  id: number;
  name: string;
  class: Class;
  fatherName: string;
  mobile1: string;
  schoolId: string;
  sections: Section[];
  section: Section;
  mobile2: string;
  address: string;
  admission: string;
  dob: Date;
  spl_fee: string;
  isFreeze;
  motherName: string;
  fee_structure_ids: number[] = [];
  class_feestructures: IMultiSelectOption[];
  constructor(id, name, sections, fee_structure_ids,
     section: Section, class_feestructure: IMultiSelectOption[], schoolId ,
     stclass: Class, fatherName, mobile1, address, mobile2, dob, isFreeze, motherName) {
    this.id = id;
    this.name = name;
    this.schoolId = schoolId;
    this.section = section;
    this.class = stclass;
    this.sections = sections;
    this.fatherName = fatherName;
    this.mobile1 = mobile1;
    this.address = address;
    this.isFreeze = isFreeze;
    this.mobile2 = mobile2;
    this.fee_structure_ids = fee_structure_ids;
    this.class_feestructures = class_feestructure;
    this.dob = dob;
    this.motherName = motherName;
  }
}
