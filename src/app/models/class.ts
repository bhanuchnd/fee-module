
export class Class {
  id: number;
  name: string;

  constructor(classId: number, className: string) {
    this.id = classId;
    this.name = className;
  }

}
