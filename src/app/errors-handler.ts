import {ErrorHandler, Injectable, Injector} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';


@Injectable()
export class ErrorsHandler implements ErrorHandler {
    constructor(private injector: Injector) {

    }
    handleError(error: Error | HttpErrorResponse) {
        if (error instanceof HttpErrorResponse) {
            if (!navigator.onLine) {
                return {isAuth: true, head: 'Offline', msg: 'No Internet Connection'};
            } else if (error.status === 401) {
                return {isAuth: false, head: 'Session Expired', msg: 'Please Login'};
            } else if (error.status === 400) {
                return {isAuth: true, head: 'Failed', msg: 'Something went wrong. Please try Again!'};
            } else if (error.status === 403) {
                return {isAuth: false, head: 'Not Authorized', msg: 'The user is not authorized to make the request'}
            } else if (error.status === 404) {
                return {isAuth: true, head: 'Not found',  msg: 'The Resource you\'re trying was not found'};
            } else if (error.status === 500) {
                return {isAuth: false, head: 'Oops',  msg: 'Unable to reach the server. Please Try Again'};
            }
        } else {
            console.log(error);
        }
    }
}
