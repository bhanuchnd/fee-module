import {Injectable} from '@angular/core';

@Injectable()
export class AppGlobals {
    createdMsg = ' is Created successfully';
    createdHead = 'Created';
    deletedMsg = ' is Deleted';
    deletedHead = 'Deleted';
    ackMsg = '';
    ackHead = '';
    confirmHead = 'Confirmation';
    deleteMsg = 'Are you sure want to delete';
    deleteHead  = 'Delete';
    errorMsg = 'Error Occured';
    serverErrorMsg = 'Cannot able to connect. Please try Again';
}

export const reportType = [
    {
    'value': 1,
    'text': 'Fee Collection Report'
    },
    {
      'value': 2,
      'text': 'Due Report'
    },
    {
      'value': 3,
      'text': 'Overdue Report'
    },
    {
      'value': 4,
      'text': 'Due & Overdue Report'
    },
    {
      'value': 5,
      'text': 'Discount Report'
    }
]

