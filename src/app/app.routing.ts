import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

import {LoginComponent} from './views/login/login.component';

export const routes: Routes = [

  {
    path: '',
    component: SimpleLayoutComponent,
    data: {
      title: 'Login'
    },
    children: [
      {
        path: '',
        loadChildren: './views/login/login.module#LoginModule',
      }
    ]
  },
  {
    path: 'home',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'transaction',
        loadChildren: './views/transaction/transaction.module#TransactionModule'
      },
      {
        path: 'discount',
        loadChildren: './views/discount/discount.module#DiscountModule'
      },
      {
        path: 'reports',
        loadChildren: './views/reports/reports.module#ReportsModule'
      },
      {
        path: 'students',
        loadChildren: './views/students/students.module#StudentsModule'
      },
      {
        path: 'communication',
        loadChildren: './views/communication/communication.module#CommunicationModule'
      },
      {
        path: 'templates',
        loadChildren: './views/templates/templates.module#TemplatesModule'
      },
      {
        path: 'mapping',
        loadChildren: './views/mapping/mapping.module#MappingModule'
      },
      {
        path: 'settings',
        loadChildren: './views/settings/settings.module#SettingsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
